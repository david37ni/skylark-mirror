﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaCms.Helpers
{
   public  class UniteCmsLookups
    {

        public Guid lookup_id { get; set; }
        public string  code { get; set; }

        public string codeName { get; set; }

        public string description { get; set; }
        public string active { get; set; }
        public int status { get; set; }
        public string auth_user { get; set; }
        public string modified_user { get; set; }
        public string modified_date { get; set; }
        public string lookup_typeId { get; set; }


    }
}
