﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SkyLarkArenaDal;

namespace SkyLarkArenaCms.Helpers
{
    public class BasePage :  System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        
    
        public SkyLarkArenaDa GetDataAccessLayer ()
        {

           
            return _dal;


        }



        Guid _UserID;
        public Guid UserID
        {
            get
            {
                return new Guid(Session["userId"].ToString());
            }
            set
            {
                this._UserID = value;
            }
        }
    

    }
}