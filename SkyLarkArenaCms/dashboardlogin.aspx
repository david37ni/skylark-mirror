﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/soccer/kidsdashboard.master" AutoEventWireup="true" CodeBehind="dashboardlogin.aspx.cs" Inherits="SkyLarkArenaCms.dashboardlogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
        
        <div class="login-box" style="width:50%;" >
     
      <div class="login-box-body">
        <p class="login-box-msg">Login to view your points!</p>
            <div class="form-group has-feedback">
                <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
                 
                   <asp:TextBox ID="txtUserName" CssClass="form-control" runat="server"></asp:TextBox>
                      
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
                           <asp:TextBox ID="txtPassword"  CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                       
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
         
            <div class="col-xs-6">
               <br />
                        <asp:Button ID="btnLogin" CssClass="btn btn-primary btn-block btn-flat" runat="server" Text="Sign In " OnClick="btnLogin_Click"  />
                         
            </div><!-- /.col -->
          </div>
   

        

        <a href="#">I forgot my password</a><br>
        <a href="register.aspx" class="text-center">Register a new membership</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
</asp:Content>
