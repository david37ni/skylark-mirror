﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Login.master" AutoEventWireup="true" CodeBehind="BackDoorLogin.aspx.cs" Inherits="SkyLarkArenaCms.BackDoorLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
     <div class="login-box">
      <div class="login-logo">
        <a href="Login.aspx"><b>Soccer</b>Admin</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
            <div class="form-group has-feedback">
                <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
                 
                   <asp:TextBox ID="txtUserName" CssClass="form-control" runat="server"></asp:TextBox>
                      
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
                           <asp:TextBox ID="txtPassword"  CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                       
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                    <asp:CheckBox ID="chkRememberMe"  Text="Remember Me" runat="server" />                  
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-6">
               <br />
                        <asp:Button ID="btnLogin" CssClass="btn btn-primary btn-block btn-flat" runat="server" Text="Sign In " OnClick="btnLogin_Click1" />
                         
            </div><!-- /.col -->
          </div>
   

        <div class="social-auth-links text-center">
          <p>- OR -</p>
            </div><!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        <a href="register.aspx" class="text-center">Register a new membership</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
      
 
</asp:Content>
