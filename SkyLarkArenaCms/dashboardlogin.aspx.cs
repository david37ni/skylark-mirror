﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using System.Web.Security;
using SkyLarkArenaDal.Models;

namespace SkyLarkArenaCms
{
    public partial class dashboardlogin : System.Web.UI.Page
    {   SkyLarkArenaDa _dal = new SkyLarkArenaDa();
     
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
               Users _loginUser = _dal.VerifyPassword(txtUserName.Text, txtPassword.Text);

               if (_loginUser == null)
                   lblerror.Text = "Invalid Login";
               else
               {


                   UserData userData = new UserData
                   {
                       fullName = _loginUser.firstName,
                       userName = _loginUser.LoweredUserName,
                       userId = _loginUser.UserId
                   };
                   
 
                   string[] roles = new string[3];

                   if (_loginUser.canAdd == true)
                       roles[0] = "canAdd";
                   if (_loginUser.canDelete == true)
                       roles[1] = "canDelete";
                   if (_loginUser.canEdit == true)
                       roles[2] = "canEdit";

                   string _roles = String.Join(",", roles);

                   FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                     1,                                     // ticket version
                     _loginUser.UserName,                              // authenticated username
                     DateTime.Now,                          // issueDate
                     DateTime.Now.AddMinutes(30),           // expiryDate
                      true,                          // true to persist across browser sessions we want to be always for end user unless they log out.
                     _roles,                 // can be used to store additional user data
                     FormsAuthentication.FormsCookiePath);  // the path for the cookie

                   // Encrypt the ticket using the machine key
                   string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                   // Add the cookie to the request to save it
                   HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                   cookie.HttpOnly = true;
                   Response.Cookies.Add(cookie);

                   lblerror.Text = "Success!";

                   // if its a player throw them into front end other wise fire them into the back end
                   if (_loginUser.accountType == Constants.playerGuid &&  _loginUser.accountType == Constants.adminGuid)
                   {
                       Response.Redirect("~/dashboard/dashboard.aspx");

                   }
                   else if (_loginUser.accountType == Constants.teamGuid && _loginUser.accountType == Constants.leagueGuid && _loginUser.accountType == Constants.clubGuid)
                   {
                       Response.Redirect("~/Backdoor/default.aspx");
                   }
               }
        }
    }
}