﻿using log4net;
using log4net.Repository.Hierarchy;
using Nemiro.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace SkyLarkArenaCms
{
    public class Global : System.Web.HttpApplication
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Global));
  
        protected void Application_Start(object sender, EventArgs e)
        {

            log4net.Config.XmlConfigurator.Configure();
        
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

            string cTheFile = HttpContext.Current.Request.Path;

            // Check if I am all ready on login page to avoid crash
            if (!cTheFile.EndsWith("login.aspx"))
            {
                // Extract the form's authentication cookie
                string cookieName = FormsAuthentication.FormsCookieName;
                HttpCookie authCookie = Context.Request.Cookies[cookieName];

                // If not logged in
                if (null == authCookie)
                // Alternative way of checking:
                //     if (HttpContext.Current.User == null || HttpContext.Current.User.Identity == null || !HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Response.Redirect("~/BackDoor/login.aspx", true);
                    Response.End();
                    return;
                }
            }

            if (Request.IsAuthenticated)
            {
                // Create an Identity object
                FormsIdentity identity = (FormsIdentity)Context.User.Identity;
                // When the ticket was created, the UserData property was assigned a
                // pipe delimited string of role names.
                string[] roles = identity.Ticket.UserData.Split(new char[] { ',' });
                String userData = identity.Ticket.UserData;
                // This principal will flow throughout the request.
                GenericPrincipal principal = new GenericPrincipal(identity, roles);
                // Attach the new principal object to the current HttpContext
                Context.User = principal;
            }

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Unhandled error occured in application. Sender: ");
            sb.AppendLine(Request.RawUrl);
            sb.Append("Query: ");
            sb.AppendLine(Request.QueryString.ToString());

            Exception ex = Server.GetLastError().GetBaseException();

            Log.Debug(ex.ToString());
       
            Server.ClearError();
        }

        protected void Session_End(object sender, EventArgs e)
        {

           
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}