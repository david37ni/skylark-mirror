﻿using SkyLarkArenaDal;
using SkyLarkArenaDal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

        
namespace SkyLarkArenaCms
{
    public partial class BackDoorLogin : System.Web.UI.Page
    {
        private SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click1(object sender, EventArgs e)
        {
            Users _loginUser = _dal.VerifyPassword(txtUserName.Text, txtPassword.Text);

            if (_loginUser == null)
                lblerror.Text = "Invalid Login";
            else
            {


                UserData userData = new UserData
                {
                    fullName = _loginUser.firstName,
                    userName = _loginUser.LoweredUserName,
                    userId = _loginUser.UserId
                };

                string[] roles = new string[3];

                if (_loginUser.canAdd == true)
                    roles[0] = "canAdd";
                if (_loginUser.canDelete == true)
                    roles[1] = "canDelete";
                if (_loginUser.canEdit == true)
                    roles[2] = "canEdit";

                string _roles = String.Join(",", roles);

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                  1,                                     // ticket version
                  _loginUser.UserName,                              // authenticated username
                  DateTime.Now,                          // issueDate
                  DateTime.Now.AddMinutes(30),           // expiryDate
                   chkRememberMe.Checked,                          // true to persist across browser sessions
                  _roles,                 // can be used to store additional user data
                  FormsAuthentication.FormsCookiePath);  // the path for the cookie

                // Encrypt the ticket using the machine key
                string encryptedTicket = FormsAuthentication.Encrypt(ticket);

                // Add the cookie to the request to save it
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                cookie.HttpOnly = true;
                Response.Cookies.Add(cookie);

                lblerror.Text = "Success!";

                Response.Redirect("default.aspx");


            }
        }

    }
}