﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/blank.master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.players.edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-style: solid;
            border-width: 3px;
        }

        .auto-style2 {
            width: 263px;
        }

        .auto-style3 {
            width: 263px;
            height: 20px;
        }

        .auto-style4 {
            height: 20px;
        }

        .auto-style5 {
            width: 263px;
            height: 17px;
        }

        .auto-style6 {
            height: 17px;
        }

        .auto-style7 {
            left: 0px;
            top: 1px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>



 
    <div class="box-body">

         
        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>

            <asp:TextBox ID="txtFullName" CssClass="form-control" runat="server"></asp:TextBox>

           </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Player Id</label>
            <asp:Label ID="lblPlayerId" CssClass="form-control" runat="server" Text="Label"></asp:Label>
        </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Player Name</label>
            <asp:Label ID="lblPlayerName" CssClass="form-control" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Gender</label>
            <br />

            <asp:DropDownList ID="dlGenders" runat="server"></asp:DropDownList>

        </div>


        <div class="form-group">
            <label for="exampleInputEmail1">Shirt Number</label>
            <br />
            <asp:TextBox ID="txtShirtNumber" CssClass="form-control" runat="server"></asp:TextBox>

        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Team</label><br />
            <asp:DropDownList ID="ddlTeam" runat="server"></asp:DropDownList>

        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">District</label>
            <br />

            <asp:DropDownList ID="ddldistrcit" runat="server"></asp:DropDownList>

        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Address</label>


            <asp:TextBox ID="txtAddress" CssClass="form-control" runat="server"></asp:TextBox>


        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Town</label>
            <asp:TextBox ID="txtTown" CssClass="form-control" runat="server"></asp:TextBox>


        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Zip Code</label>

            <asp:TextBox ID="txtZipCode" CssClass="form-control" runat="server"></asp:TextBox>


        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Telephone</label>
            <asp:TextBox ID="txtTelphone" CssClass="form-control" runat="server"></asp:TextBox>

        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>

       
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Description</label>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">

                    <textarea id="txtDescription" runat="server" name="txtDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Date OF Brith</label>
            <div class="input-group date">
                <asp:TextBox ID="dateOfBirth" data-provide="datepicker" data-date-format='dd/mm/yyyy' CssClass="form-control" runat="server"></asp:TextBox><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>




        </div>



        <div class="form-group">
            <a name="playersPoints"></a>

            <div class="form-group">

                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn  btn-info" OnClick="btnCancel_Click" />


            </div>

        </div>
    </div>



</asp:Content>
