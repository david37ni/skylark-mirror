﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.players._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
 
            
              <div class="title_left">
                            <h3>Players Managment</h3>
                        </div>
                   
    <asp:ScriptManager ID="sptgrdSoccerPlayers" runat="server"></asp:ScriptManager>
      
    <asp:Button ID="btnAddNewPlayer" runat="server" CssClass="btn btn-info btn-lg" Text="Add New Player" OnClick="btnAddNewPlayer_Click" />
    
    
    <asp:GridView ID="grdSoccerPlayers"   runat="server"  CssClass= "table table-striped table-bordered table-condensed"  DataKeyNames="id" AutoGenerateColumns="False"  Visible="true" OnRowCommand="grdSoccerPlayers_RowCommand"  OnRowDataBound="grdSoccerPlayers_RowDataBound" >
    <EmptyDataTemplate
        >
       <div class="alert alert-info alert">
                      <h4><i class="icon fa fa-info"></i> Alert!</h4>
                    Their are no players in the system. No User has registered as a player.
                  </div>


    </EmptyDataTemplate>
            <Columns>
                 
               
            <asp:BoundField DataField="id" Visible="false" HeaderText="id" ReadOnly="True" SortExpression="id" />
            
             <asp:BoundField DataField="player_id" HeaderText="Player Id" ReadOnly="True" SortExpression="player_id" />
                     <asp:BoundField DataField="name" Visible="true" HeaderText="Name" ReadOnly="True" SortExpression="name" />
          
             <asp:BoundField DataField="dob" DataFormatString="{0:dd/M/yyyy}"  HeaderText="DOB" ReadOnly="True" SortExpression="dob" />
           
            <asp:BoundField DataField="address" HeaderText="Address" ReadOnly="True" SortExpression="address" />
            <asp:BoundField DataField="town" HeaderText="Town" ReadOnly="True" SortExpression="town" />
            <asp:BoundField DataField="gender" HeaderText="Gender" ReadOnly="True" SortExpression="gender" />
            <asp:BoundField DataField="zipcode" HeaderText="Zip Code" ReadOnly="True" SortExpression="zipcode" />
              <asp:CheckBoxField DataField="active" HeaderText="Active" ReadOnly="True" SortExpression="active" />
            <asp:BoundField DataField="createdDate" DataFormatString="{0:dd/M/yyyy}"  HeaderText="Date" ReadOnly="True" SortExpression="createdDate" />
            <asp:BoundField DataField="teamId"  HeaderText="Team Name" ReadOnly="True" SortExpression="teamId" />
              <asp:ButtonField CommandName="Edit" Text="Edit" />
        <asp:ButtonField CommandName="DeletePlayer" Text="Delete" />
        </Columns>
              </asp:GridView>

     
     
        <br />
        
 
          

</asp:Content>
