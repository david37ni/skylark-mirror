﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using System.Diagnostics;
using System.Data;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using log4net;
namespace SkyLarkArenaCms.BackDoor.players
{
    public partial class edit : System.Web.UI.Page
    {


        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        Guid _playerId;
        Guid _teamId;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
                if (m != null) m.themeTitle = "Players Edit";

                player _player = new player();

                string id = Request.QueryString["id"];

                Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


                _player = _dal.GetPlayerBYID(_guid);


                if (!Page.IsPostBack)
                {
                    if (_player.Name != null)
                        txtFullName.Text = _player.Name;
                    if (_player.address != null)
                        txtAddress.Text = _player.address;

                    if (_player.player_id != null)
                        lblPlayerId.Text = _player.player_id.ToString();

                    if (_player.email != null)
                        txtEmail.Text = _player.email.ToString();


                    if (_player.description != null)
                        txtDescription.InnerText = _player.description;

                    if (_player.dob != null)
                        dateOfBirth.Text = Convert.ToString(_player.dob);
                    if (_player.player_id != null)
                        lblPlayerId.Text = _player.player_id;

                    if (_player.shirtNumber != null)
                        lblPlayerId.Text = _player.shirtNumber.ToString();

                    dlGenders.SelectedValue = _player.gender;

                    if (_player.description != null)
                        txtDescription.InnerText = _player.description;


                    if (_player.shirtNumber != null)
                        txtShirtNumber.Text = _player.shirtNumber.ToString();
                    else
                        txtShirtNumber.Text = "0";
                    if (_player.player_id != null)                
                    lblPlayerId.Text = _player.player_id.ToString();

                 

                }



                //if (_player.player_points.Count > 0)
                //{

                //    grdPoints.DataSource = _dal.getAllPlayersPointsByPlayerId(_guid);
                //    grdPoints.MasterTableView.DataKeyNames = new string[] { "playerId" };
                //    grdPoints.DataBind();


                //}

                var dlGendersSource = _dal.GetGenders();
                dlGenders.DataSource = dlGendersSource;
                dlGenders.DataValueField = "LookupValue";
                dlGenders.DataTextField = "LookupDescription";
                dlGenders.DataBind();


                var dlteams = _dal.GetTeams();
                ddlTeam.DataSource = dlteams;
                ddlTeam.DataValueField = "LookupValue";
                ddlTeam.DataTextField = "LookupDescription";
                ddlTeam.DataBind();

                var dldistrcits = _dal.GetDistricts();

                ddldistrcit.DataSource = dldistrcits;
                ddldistrcit.DataValueField = "Prefix";
                ddldistrcit.DataTextField = "Name";
                ddldistrcit.DataBind();

                dlGenders.Items.Add(new ListItem("Please Select", "0", true));
                ddlTeam.Items.Add(new ListItem("Please Select", "0", true));
                ddldistrcit.Items.Add(new ListItem("Please Select", "0", true));
   

            }
            catch (Exception ex)
            {
                throw new EntityContextException("Page Load Failed in Edit Players  .", ex);
            }


        }



        protected void btnSave_Click(object sender, EventArgs e)
        {


            //try
            //{


                string id = Request.QueryString["id"];
                if (id != "00000000-0000-0000-0000-000000000000")
                {
                    _playerId = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

                }

                string teamIds = Request.QueryString["teamId"];
                if (teamIds != "00000000-0000-0000-0000-000000000000")
                {
                    _teamId = string.IsNullOrEmpty(teamIds) ? new Guid() : new Guid(teamIds);

                }

                player _player = new player();





                _player = _dal.GetPlayerBYID(_playerId);

                _player.Name = txtFullName.Text;
                _player.email = txtEmail.Text;
                _player.address = txtAddress.Text;

                _player.description = txtDescription.InnerText;
                _player.gender = dlGenders.SelectedValue.ToString();
                _player.modifiedDate = DateTime.Now;
                _player.teamId = new Guid(ddlTeam.SelectedValue.ToString());
                _player.description = txtDescription.InnerText;

                _player.shirtNumber = Convert.ToInt32(txtShirtNumber.Text);

                _player.createdDate = DateTime.Now;           

            DateTime? _dob = (String.IsNullOrEmpty(dateOfBirth.Text) ? (DateTime?)null : DateTime.Parse(dateOfBirth.Text));

             
                    _player.dob =_dob;
                    _player.isDeleted = false;

                if (_player.player_id == null)
                    _player.player_id = _dal.genPlayerID(new Guid("B9B2A89A-2295-4E48-BBA4-9DC28A1855FB"), new Guid("6E424913-A9A4-4A46-9785-C8AFA09C82DB"));


                if (_teamId != Guid.Empty)
                {
                    _player.teamId = _teamId;

                }




                   if (_player.player_id  !="")               
                   _dal.SkyLarkArenaEntities.players.Add(_player);




                   _dal.SkyLarkArenaEntities.SaveChanges();
            //}

            //catch (Exception ex)
            //{

            //    throw new EntityContextException("btnSave_Click failed.", ex);

            //}

            Response.Redirect("default.aspx?teamId=00000000-0000-0000-0000-000000000000&id=00000000-0000-0000-0000-000000000000");



        }




        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }



    }
}