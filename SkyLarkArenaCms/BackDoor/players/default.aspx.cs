﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using SkyLarkArenaDal.Models;
using log4net;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;

using Humanizer;
namespace SkyLarkArenaCms.BackDoor.players
{
    public partial class _default :BasePage
    {

        SkyLarkArenaDa _dal;
        Guid _guid;
        protected void Page_Load(object sender, EventArgs e)
        {

            _dal = GetDataAccessLayer();

            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Players List";


            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;

            string id = Request.QueryString["teamId"];
            if (id != "00000000-0000-0000-0000-000000000000")
            {
                _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


                grdSoccerPlayers.DataSource = _dal.getAllPlayersByTeamId(_guid);


            }
            else
            {
                grdSoccerPlayers.DataSource = _dal.getAllPlayers(UserID);


            }


            grdSoccerPlayers.DataBind();
            

 


        }


       
 

        protected void btnAddNewPlayer_Click(object sender, EventArgs e)
        {
            string strid = Request.QueryString["teamId"];

             Response.Redirect("edit.aspx?teamId=" + Guid.Empty.ToString() + "&id=" + Guid.Empty.ToString());
        }

        protected void grdSoccerPlayers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdSoccerPlayers.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }

            if (e.CommandName == "DeletePlayer")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdSoccerPlayers.DataKeys[index].Values["id"].ToString();






                player _player = _dal.GetPlayerBYID(new Guid(value));
                _player.isDeleted = true;
                _player.deletedDate = DateTime.Now;
                  _dal.SkyLarkArenaEntities.SaveChanges();

                  Response.Redirect("default.aspx?id=00000000-0000-0000-0000-000000000000&teamId=00000000-0000-0000-0000-000000000000");
            


               
            }

        }

  
        protected void grdSoccerPlayers_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            player drv = (player)e.Row.DataItem;

           
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (drv.teamId != null && drv.teamId != Guid.Empty)
                {

                    Guid rowTeamid = new Guid(drv.teamId.ToString());
                    team teamName = _dal.SkyLarkArenaEntities.teams.Where(res => res.id == rowTeamid).First();
                    e.Row.Cells[10].Text = teamName.name;

                }


                if (drv.createdDate !=null)
                {
                    DateTime dt;
                    dt = Convert.ToDateTime("11/1/2015");
                    string date = dt.Humanize();

                    e.Row.Cells[9].Text = date;



                }

                if (drv.gender != null && drv.gender != string.Empty)
                {

                    string genderId =drv.gender.ToString();
                    lookupValue genderName = _dal.SkyLarkArenaEntities.lookupValues.Where(res => res.lookupValue1 == genderId).First();
                    e.Row.Cells[6].Text = genderName.lookupDescription;

                }


            }
        }


    }
}