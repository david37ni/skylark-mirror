﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;

namespace SkyLarkArenaCms.BackDoor.playerpoints
{
    public partial class _default : System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();

        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Players Points List";

            grdPoints.DataSource = _dal.getAllPoints();

            grdPoints.DataBind();

        }

        protected void grdPoints_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdPoints.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }

            if (e.CommandName == "DeletePoints")
            {

                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdPoints.DataKeys[index].Values["id"].ToString();

                player_points _points = new player_points();
                _points = _dal.GetPointsById(new Guid(value));
                _points.isDeleted = true;
                _points.deletedDate = DateTime.Now;

                _dal.SkyLarkArenaEntities.SaveChanges();
                

               



            }
        }

        protected void btnAddNewPoints_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
      
        }

        protected void grdPoints_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            player_points drv = (player_points)e.Row.DataItem;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {



                if (drv.playerId != null && drv.playerId != Guid.Empty)
                {

                    Guid rowPlayer = new Guid(drv.playerId.ToString());
                    player playerName = _dal.SkyLarkArenaEntities.players.Where(res => res.id == rowPlayer).First();
                    e.Row.Cells[1].Text = playerName.Name;
                }
            }
        }

    }
}