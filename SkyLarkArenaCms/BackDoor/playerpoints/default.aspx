﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.playerpoints._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
    
    
                     <asp:ScriptManager ID="sptgrdSoccerPlayers" runat="server"></asp:ScriptManager>
     <asp:Button ID="btnAddNewPoints" runat="server" CssClass="btn btn-info btn-lg" Text="Add New Points" OnClick="btnAddNewPoints_Click"  />
    
      <asp:GridView ID="grdPoints" runat="server" DataKeyNames="id" AutoGenerateColumns="False" CssClass= "table table-striped table-bordered table-condensed"  OnRowCommand="grdPoints_RowCommand" OnRowDataBound="grdPoints_RowDataBound">
        <Columns>
            <asp:BoundField DataField="id" Visible="false" HeaderText="id" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="playerId" HeaderText="Player Name" ReadOnly="false" SortExpression="playerId" />
            <asp:BoundField DataField="points" HeaderText="Existing Points" ReadOnly="True" SortExpression="points" />
            <asp:BoundField DataField="pointsToRedeem" HeaderText="Add Points" ReadOnly="True" SortExpression="pointsToRedeem" />
             <asp:CheckBoxField DataField="approved" HeaderText="Approved" ReadOnly="True" SortExpression="approved" />             
                <asp:BoundField DataField="approvedDate" DataFormatString="{0:dd/M/yyyy}" HeaderText="Approved Date" ReadOnly="True" SortExpression="approvedDate" />
               <asp:ButtonField CommandName="Edit" Text="Edit" />
            <asp:ButtonField CommandName="DeletePoints" Text="Delete" />
        </Columns>
        
        <EmptyDataTemplate>
    <div class="alert alert-info alert">
                      <h4><i class="icon fa fa-info"></i> Alert!</h4>
                    Their are no points in the system. No User has asked for redemed points.
                  </div>

        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
