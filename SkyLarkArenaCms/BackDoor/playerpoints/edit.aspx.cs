﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
namespace SkyLarkArenaCms.BackDoor.playerpoints
{
       

    public partial class edit : System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        Guid _guid;
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Players Points Edit";


            player_points _playerPointsds;
            string id = Request.QueryString["id"];
            if (id != "00000000-0000-0000-0000-000000000000")
            {
                _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

          
            }



            _playerPointsds = _dal.GetPointsTORedeemByPlayerId(_guid);
            if (id != "00000000-0000-0000-0000-000000000000")
            {
                player playerName = _dal.SkyLarkArenaEntities.players.Where(res => res.id == _playerPointsds.playerId).First();
                lblplayerName.Text = playerName.Name;

            }
            if (!Page.IsPostBack)
            {
                if (_playerPointsds.playerName != null)
                    lblplayerName.Text = _playerPointsds.playerName;



                if (_playerPointsds.playerNumber != null)
                    lblplayerNumber.Text = _playerPointsds.playerNumber.ToString();

                if (_playerPointsds.points != null)
                    lblpoints.Text = _playerPointsds.points.ToString();

                if (_playerPointsds.pointsToRedeem != null)
                    lblpointstoredeem.Text = _playerPointsds.pointsToRedeem.ToString();


            }



        


        }

        protected void chkApproved_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void btnPlayerPoitnsUpdate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/BackDoor/players/edit.aspx?id=" + Request.QueryString["id"] + "#playersPoints");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}