﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.channel
{
    public partial class _default :BasePage
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
    

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Context.User.Identity.AuthenticationType == "Forms" && Context.User.Identity.IsAuthenticated)
            {
                FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
                _identity = (FormsIdentity)Context.User.Identity;
            

                grdChannels.DataSource = _dal.getAllChannels(UserID);

                grdChannels.DataBind();
            }

        }

        protected void grdChannels_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdChannels.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }

        }

        protected void btAddNewChannel_Click(object sender, EventArgs e)
        {
             

            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
 
        }

       
    }
}