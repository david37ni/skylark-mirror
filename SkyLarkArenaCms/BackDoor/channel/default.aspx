﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.channel._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">

      <asp:ScriptManager ID="sptgrdSoccerPlayers" runat="server"></asp:ScriptManager>
    <asp:GridView ID="grdChannels"  CssClass= "table table-striped table-bordered table-condensed" runat="server" DataKeyNames="id" AutoGenerateColumns="False" OnRowCommand="grdChannels_RowCommand">
       <EmptyDataTemplate>

              <asp:Button ID="btAddNewChannel" runat="server" CssClass="btn btn-info btn-lg" Text="Add New Channel" OnClick="btAddNewChannel_Click" /> <br />
    
    
                 <div class="alert alert-info alert">
                      <h4><i class="icon fa fa-info"></i> Alert!</h4>
                    Their are no Channels in the system. No User has registered a Channel.
                  </div>
       </EmptyDataTemplate>
        <Columns>
             <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="false" SortExpression="id" />
            <asp:BoundField DataField="name" HeaderText="name" ReadOnly="True" SortExpression="name" />
               <asp:BoundField DataField="authCreatedDate" HeaderText="Created Date" ReadOnly="True" SortExpression="name" />
             
            
            
            
            
                 <asp:ButtonField CommandName="Edit" Text="Edit" />
        <asp:ButtonField CommandName="DeleteDistrict" Text="Delete" />
        </Columns>
    </asp:GridView>
       
        <br />
</asp:Content>
