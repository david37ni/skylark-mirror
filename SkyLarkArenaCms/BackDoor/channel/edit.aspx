﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.channel.edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">


     
 <div class="box-body">

           <div class="form-group">
                 <label for="exampleInputEmail1">Channel Name</label><br />
                
                <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Width="425px"></asp:TextBox>
          </div>

         <div class="form-group">
                 <label for="exampleInputEmail1">Channel Image</label><br />
             <asp:FileUpload ID="channelImage"   CssClass="form-control" runat="server" />
           </div>

     

        <div class="form-group">
            <label for="exampleInputEmail1">Channel Description</label>

            <div class="box">
                <div class="box-header">
                      <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">

                    <textarea id="txtDescription" runat="server" name="txtDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                </div>
            </div>
        </div>


     <div class="form-group">
           <asp:Button ID="btnSave" runat="server"  CssClass="btn btn-info" Text="Save" OnClick="btnSave_Click" />
                    <asp:Button ID="btCancel" CssClass="btn  btn-info" runat="server" Text="Cancel" OnClick="btCancel_Click" />
         </div> 
     </div>
    
</asp:Content>
