﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal.Helpers;
using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.teams
{
    public partial class _default : BasePage
    {
        SkyLarkArenaDa _dal;
        protected void Page_Load(object sender, EventArgs e)
        {

            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Teams List";

            _dal = GetDataAccessLayer();

           
            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;

            grdTeam.DataSource = _dal.getAllTeams(UserID);
            grdTeam.DataBind();


        }

        protected void grdTeam_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdTeam.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }

            if (e.CommandName == "EditPlayer")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string teamId = this.grdTeam.DataKeys[index].Values["id"].ToString();


                Response.Redirect("~/BackDoor/players/default.aspx?teamId=" + teamId.ToString() + "&id=" + Guid.Empty.ToString());
            }
        }

        protected void grdTeam_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            team drv = (team)e.Row.DataItem;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {



                if (drv.districtId != null && drv.districtId != Guid.Empty)
                {

                    Guid rowDistrtict = new Guid(drv.districtId.ToString());
                      district districtName = _dal.SkyLarkArenaEntities.districts.Where(res => res.id == rowDistrtict).First();
                    e.Row.Cells[2].Text = districtName.name;
                   }
                if (drv.leagueId != null && drv.leagueId != Guid.Empty)
                {

                    Guid teamLeague = new Guid(drv.leagueId.ToString());
                    Leauge leageName = _dal.SkyLarkArenaEntities.Leauges.Where(res => res.id == teamLeague).First();
                    e.Row.Cells[3].Text = leageName.LeagueName;

                }

            }
        }

        //protected void grdTeams_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        //{
        //    if (e.CommandName == RadGrid.InitInsertCommandName)
        //    {

        //        Guid strId = Guid.Empty;
        //        Response.Redirect("edit.aspx?id=" + strId.ToString());

        //    }

        //    if (e.CommandName == "Delete")
        //    {
        //        GridDataItem item = e.Item as GridDataItem;
        //        Guid strId = new Guid(item.GetDataKeyValue("id").ToString());
        //        team _team = _dal.SkyLarkArenaEntities.teams.FirstOrDefault(p => p.id == strId);
        //        if (_team != null)
        //        {

        //            if (_team.players != null && _team.players.Count > 0)
        //            {
        //                var _palayers = _team.players.ToList();
        //                foreach (player _player in _palayers)
        //                {
        //                    _dal.SkyLarkArenaEntities.players.DeleteObject(_player);
        //                }
        //            }
        //            _dal.SkyLarkArenaEntities.teams.DeleteObject(_team);
        //        }
        //        _dal.SkyLarkArenaEntities.SaveChanges();


        //    }
        //    if (e.CommandName == "Edit")
        //    {
        //        GridDataItem item = e.Item as GridDataItem;
        //        Guid strId = new Guid(item.GetDataKeyValue("id").ToString());
        //        Response.Redirect("edit.aspx?id=" + strId.ToString());

        //    }
        //        if (e.CommandName == "EditTeamPlayers")
        //        {

        //            GridDataItem item = e.Item as GridDataItem;
        //            Guid strId = new Guid(item.GetDataKeyValue("id").ToString());
        //            Response.Redirect("~/BackDoor/players/default.aspx?teamId=" + strId.ToString());

        //        }
        //        if (e.CommandName == RadGrid.InitInsertCommandName)
        //        {
        //            Response.Redirect("edit.aspx");

        //        }

        //}
    }
}