﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.teams.edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        border-style: solid;
        border-width: 3px;
    }
        .auto-style2 {
        }
        .auto-style3 {
            width: 188px;
            height: 24px;
        }
        .auto-style4 {
            height: 24px;
        }
        .auto-style5 {
            height: 19px;
        }
        .auto-style6 {
            width: 188px;
        }
        .auto-style7 {
            height: 19px;
            width: 188px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
  
 
 <div class="box-body">
     

            <div class="form-group">
                 <label for="exampleInputEmail1">Name</label><br />
                
          
            <asp:TextBox ID="txtName"  CssClass="form-control"   runat="server" Width="383px"></asp:TextBox>
       </div>

     
      
      <div class="form-group">
            <label for="exampleInputEmail1">Description</label>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">

                    <textarea id="txtDescription" runat="server" name="txtDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                </div>
            </div>
        </div>
       

            <div class="form-group">
                 <label for="exampleInputEmail1">District</label><br />
                <asp:DropDownList ID="ddlDistrcts" runat="server"></asp:DropDownList>
                 
        </div>
          

            <div class="form-group">
                 <label for="exampleInputEmail1">League</label><br />
                      <asp:DropDownList ID="RdLeague" runat="server"></asp:DropDownList>
         
                    
        </div>
       

            <div class="form-group">
                 <label for="exampleInputEmail1">Active</label><br />
                
         <asp:CheckBox ID="chkactive" runat="server" CssClass="form-control"   />
      </div>
       

            <div class="form-group">
                <asp:Button ID="btnSave"   CssClass="btn  btn-info" runat="server" Text="Save"   OnClick="btnSave_Click"/>
                  <asp:Button ID="btnCancel"  CssClass="btn  btn-info" runat="server" Text="Cancel" OnClick="btnCancel_Click"  />
              
      </div>
     </div>

</asp:Content>
