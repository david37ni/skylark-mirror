﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.teams._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
  

    
<asp:GridView ID="grdTeam" runat="server" DataKeyNames="id"  CssClass= "table table-striped table-bordered table-condensed" AutoGenerateColumns="False"  OnRowCommand="grdTeam_RowCommand" OnRowDataBound="grdTeam_RowDataBound">
    <Columns>
        <asp:BoundField DataField="id" Visible="false" HeaderText="id" ReadOnly="True" SortExpression="id" />
           
        <asp:BoundField DataField="name" HeaderText="Team Name" ReadOnly="True" SortExpression="name" />
        <asp:BoundField DataField="districtId" HeaderText="District" ReadOnly="True" SortExpression="districtId" />
        <asp:BoundField DataField="leagueId" HeaderText="League Id" ReadOnly="True" SortExpression="leagueId" />
        <asp:BoundField DataField="dateCreated" HeaderText="Date" ReadOnly="True" SortExpression="dateCreated" />
        <asp:CheckBoxField DataField="isActive" HeaderText="Active" ReadOnly="True" SortExpression="isActive" />
        
         <asp:ButtonField CommandName="EditPlayer" Text="Edit Players" />
        <asp:ButtonField CommandName="Edit" Text="Edit" />
        <asp:ButtonField CommandName="Delete" Text="Delete" />
    </Columns>
    </asp:GridView>

  
</asp:Content>
