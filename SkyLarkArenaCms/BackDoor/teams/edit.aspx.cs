﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal.Models;
using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
namespace SkyLarkArenaCms.BackDoor.teams
{
    public partial class edit : System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();


        public Guid TeamId { get; set; }
        bool isSaved = false;

        protected void Page_Load(object sender, EventArgs e)
        {


            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Teams Edit";
                     
            string id = Request.QueryString["teamId"];
            if (id != "00000000-0000-0000-0000-000000000000")
            {
                TeamId = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);
                }


            team _team = _dal.GetTeamByTeamId(TeamId);
            if (!Page.IsPostBack)
            {
                txtName.Text = _team.name;
                txtDescription.InnerText = _team.description;



            }

          





            var ddlDistrctsSource = _dal.GetDistricts();
            ddlDistrcts.DataSource = ddlDistrctsSource;
            ddlDistrcts.DataValueField = "Prefix";
            ddlDistrcts.DataTextField = "Name";
            ddlDistrcts.DataBind();



            
            var ddlLeagues = _dal.GetLeagues();
            RdLeague.DataSource = ddlLeagues;
            RdLeague.DataValueField = "LookupValue";
            RdLeague.DataTextField = "LookupDescription";
            RdLeague.DataBind();

            ddlDistrcts.Items.Add(new ListItem("Please Select", "0", true));

            RdLeague.Items.Add(new ListItem("Please Select", "0", true));
          

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {


            team _team = new team();
            if(isSaved==false)
              

            _team.isActive = true;



            Guid id = new Guid(Request.QueryString["id"].ToString());
            if (id == Guid.Empty)
            {
                id = Guid.NewGuid();

            }

            TeamId = id;

            //add it it to the tracker.

            _dal.SkyLarkArenaEntities.teams.Add(_team);


            _dal.SkyLarkArenaEntities.SaveChanges();

            Response.Redirect("default.aspx?teamID=" + TeamId.ToString());




        }

        protected void btnAddPlayer_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }

}