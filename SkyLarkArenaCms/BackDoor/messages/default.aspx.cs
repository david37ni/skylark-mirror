﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.messages
{
    public partial class _default : BasePage
    {
        SkyLarkArenaDa _dal;
        protected void Page_Load(object sender, EventArgs e)
        {

            
            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;

            _dal = GetDataAccessLayer();
       
            lblCount.Text = _dal.GetUserMessageCount(UserID).ToString();
            rptMail.DataSource = _dal.getAllMessages(UserID);
            rptMail.DataBind();
        }
    }
}