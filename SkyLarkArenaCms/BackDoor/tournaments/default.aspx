﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.tournaments._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">

      <asp:ScriptManager ID="sptgrdSoccerPlayers" runat="server"></asp:ScriptManager>
  
      


      <asp:GridView ID="grdtournments"   runat="server"  CssClass= "table table-striped table-bordered table-condensed"  DataKeyNames="id" AutoGenerateColumns="False"  Visible="true" OnRowCommand="grdtournments_RowCommand"  OnRowDataBound="grdtournments_RowDataBound" >
    <EmptyDataTemplate>

         <asp:Button ID="btnAddnewtornaments" runat="server" CssClass="btn btn-info btn-lg" Text="Add New Tournemants" OnClick="btnAddnewtornaments_Click" />
   
       <div class="alert alert-info alert">
                      <h4><i class="icon fa fa-info"></i> Alert!</h4>
                    Their are no tournaments in the system. No User has registered tournament.
                  </div>


    </EmptyDataTemplate>
            <Columns>
                 
               
            <asp:BoundField DataField="id" Visible="false" HeaderText="id" ReadOnly="True" SortExpression="id" />
            
             <asp:BoundField DataField="name" HeaderText="name Id" ReadOnly="True" SortExpression="name" />

                  <asp:BoundField DataField="tournamentsDescription" HeaderText="Description" ReadOnly="True" SortExpression="tournamentsDescription" />
           
                  <asp:BoundField DataField="name" HeaderText="name Id" ReadOnly="True" SortExpression="name" />
           
                <asp:BoundField DataField="StarDate" DataFormatString="{0:dd/M/yyyy}"  HeaderText="Start Date" ReadOnly="True" SortExpression="StarDate" />
            <asp:BoundField DataField="EndDate" DataFormatString="{0:dd/M/yyyy}"  HeaderText="End Date" ReadOnly="True" SortExpression="EndDate" />
           
            <asp:CheckBoxField DataField="isActive" HeaderText="Active" ReadOnly="True" SortExpression="isActive" />
             
            
                
                
                           <asp:ButtonField CommandName="Edit" Text="Edit" />
        <asp:ButtonField CommandName="DeletePlayer" Text="Delete" />
        </Columns>
              </asp:GridView>

     
</asp:Content>
