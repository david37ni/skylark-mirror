﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.tournaments
{
    public partial class _default :BasePage
    {
        SkyLarkArenaDa _dal ;

        tournament _tournament = new tournament();
      
        protected void Page_Load(object sender, EventArgs e)
        {

            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Tournaments List";

            _dal = GetDataAccessLayer();

            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;

            grdtournments.DataSource = _dal.getAllTorunments(UserID);
            grdtournments.DataBind();

        }

        protected void grdtournments_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdtournments_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdtournments.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }


            if (e.CommandName == "DeleteDistrict")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdtournments.DataKeys[index].Values["id"].ToString();


                _tournament = _dal.GetTorunamentById(new Guid(value));
                _tournament.isDeleted = true;
                _tournament.deletedDate = DateTime.Now;
                _dal.SkyLarkArenaEntities.SaveChanges();

                Response.Redirect("default.aspx");

            }

        }

        protected void btnAddnewtornaments_Click(object sender, EventArgs e)
        {
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
        }
    }
}