﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.tournaments.edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div class="box-body">


        <div class="form-group">
            <label for="exampleInputEmail1">Name</label><br />

            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Width="425px"></asp:TextBox>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Description</label>

            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">

                    <textarea id="txtDescription" runat="server" name="txtDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                </div>
            </div>
        </div>



        <div class="form-group">
            <label for="exampleInputEmail1">Start Date</label>
            <div class="input-group date">
                <asp:TextBox ID="startDate" data-provide="datepicker" data-date-format='dd/mm/yyyy' CssClass="form-control" runat="server"></asp:TextBox><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">End Date</label>
            <div class="input-group date">
                <asp:TextBox ID="endDate" data-provide="datepicker" data-date-format='dd/mm/yyyy' CssClass="form-control" runat="server"></asp:TextBox><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>


            <div class="form-group">
                <label for="exampleInputEmail1">Final Entry Date</label>
                <div class="input-group date">
                    <asp:TextBox ID="txtfEntryDate" data-provide="datepicker" data-date-format='dd/mm/yyyy' CssClass="form-control" runat="server"></asp:TextBox><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Active</label>
                    <div>
                        <asp:CheckBox ID="chkActive" runat="server" />


                    </div>

                </div>

            </div>




            <div class="form-group">
                <a name="playersPoints"></a>

                <div class="form-group">

                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn  btn-info" OnClick="btnCancel_Click" />


                </div>

            </div>
        </div>
</asp:Content>
