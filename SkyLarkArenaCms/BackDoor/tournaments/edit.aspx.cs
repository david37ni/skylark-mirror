﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;

namespace SkyLarkArenaCms.BackDoor.tournaments
{
    public partial class edit : System.Web.UI.Page
    {

        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Tournaments Edit";

            tournament _tournament = new tournament();

            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


           
                _tournament = _dal.GetTorunamentById(_guid);


                if (!Page.IsPostBack)
                {

                    txtName.Text = _tournament.name;
                    txtDescription.InnerText = _tournament.tournamentsDescription;
                    chkActive.Checked = Convert.ToBoolean(_tournament.isActive);


                }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);



            tournament _tournament = new tournament();
            _tournament = _dal.GetTorunamentById(_guid);


            _tournament.name = txtName.Text;
            _tournament.tournamentsDescription = txtDescription.InnerText;
             DateTime? _startDate = (String.IsNullOrEmpty(startDate.Text) ? (DateTime?)null : DateTime.Parse(startDate.Text));
            _tournament.StarDate = _startDate;
            
             DateTime? _endDate = (String.IsNullOrEmpty(endDate.Text) ? (DateTime?)null : DateTime.Parse(endDate.Text));
            _tournament.EndDate = _endDate;

            DateTime? _fentryDate = (String.IsNullOrEmpty(txtfEntryDate.Text) ? (DateTime?)null : DateTime.Parse(txtfEntryDate.Text));
            _tournament.FinalEntryDate = _fentryDate;


            _tournament.isActive = chkActive.Checked;
            if(_dal.Entry(_tournament).State == System.Data.Entity.EntityState.Detached)
                _dal.SkyLarkArenaEntities.tournaments.Add(_tournament);


            _dal.SkyLarkArenaEntities.SaveChanges();


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
           
                
        }
    }
}