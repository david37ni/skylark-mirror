﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;

namespace SkyLarkArenaCms.BackDoor.games
{
    public partial class edit : System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Games Edit";

            game _game = new game();

            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

          

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


            game _game = new game();
            _game = _dal.GetGamesById(_guid);


            _game.name = txtName.Text;
            _game.description = txtDescription.InnerText;





            if (_dal.Entry(_game).State == System.Data.Entity.EntityState.Detached)
                _dal.SkyLarkArenaEntities.games.Add(_game);


            _dal.SkyLarkArenaEntities.SaveChanges();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}