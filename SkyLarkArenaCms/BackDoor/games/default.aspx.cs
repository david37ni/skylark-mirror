﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;

namespace SkyLarkArenaCms.BackDoor.games
{
    public partial class _default : System.Web.UI.Page
    {
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
    
        protected void Page_Load(object sender, EventArgs e)
        {

            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Games List";

        }

        protected void btnAddnewGames_Click(object sender, EventArgs e)
        {
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
        }

        protected void grdGames_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdGames.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }


            if (e.CommandName == "DeleteDistrict")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdGames.DataKeys[index].Values["id"].ToString();

                tournament _tournament = _dal.GetTorunamentById(new Guid(value));
             
                   _tournament.isDeleted = true;
                _tournament.deletedDate = DateTime.Now;
                _dal.SkyLarkArenaEntities.SaveChanges();

                Response.Redirect("default.aspx");

            }
        }

        protected void grdGames_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}