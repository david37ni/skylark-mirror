﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.prizes._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>
       <asp:Button ID="btnAddNewPrizes" runat="server" CssClass="btn btn-info btn-lg" Text="Add New Prizes" OnClick="btnAddNewPrizes_Click"  />
    
    <asp:GridView ID="grdPrizes"  runat="server" DataKeyNames="id" CssClass= "table table-striped table-bordered table-condensed"  AutoGenerateColumns="False" DataSourceID="entPrizes" OnRowCommand="grdPrizes_RowCommand" OnRowDataBound="grdPrizes_RowDataBound">
        <Columns>
               <asp:BoundField DataField="id" Visible="false" HeaderText="id"  ReadOnly="True" SortExpression="id" />
         
            <asp:BoundField DataField="prizeName" HeaderText="Name" ReadOnly="True" SortExpression="prizeName" />
            <asp:BoundField DataField="points" HeaderText="Points Payout" ReadOnly="True" SortExpression="points" />
            <asp:BoundField DataField="foundation" HeaderText="Foundation Payout" ReadOnly="True" SortExpression="foundation" />
            <asp:BoundField DataField="kids" HeaderText="Kids Payout" ReadOnly="True" SortExpression="kids" />
            <asp:BoundField DataField="totalpayout" HeaderText="Total Payout" ReadOnly="True" SortExpression="totalpayout" />
               <asp:BoundField DataField="authCreatedDate" HeaderText="Date" DataFormatString="{0:dd/M/yyyy}" ReadOnly="True" SortExpression="authCreatedDate" />
            <asp:CheckBoxField DataField="isActive" HeaderText="Active" ReadOnly="True" SortExpression="isActive" />
               
            <asp:ButtonField CommandName="Edit"  Text="Edit" />
            <asp:ButtonField CommandName="DeletePrizes" Text="Delete" />
        </Columns>
     </asp:GridView>
    <asp:EntityDataSource ID="entPrizes" runat="server" ConnectionString="name=UniteCmsEntities" DefaultContainerName="UniteCmsEntities" EnableFlattening="False" EntitySetName="prizes" Select="it.[prizeName], it.[points], it.[foundation], it.[kids], it.[totalpayout], it.[gift], it.[gifImage], it.[isActive], it.[authUserName], it.[authCreatedDate], it.[id]">
</asp:EntityDataSource>
</asp:Content>
