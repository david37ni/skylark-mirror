﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SkyLarkArenaCms.BackDoor.Themes.fluid;
using log4net;
using SkyLarkArenaDal;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.prizes
{

    public partial class _default : BasePage

    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Global));
        SkyLarkArenaDa _dal;
    
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Prizes List";

            _dal = GetDataAccessLayer();
        }

        protected void grdPrizes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdPrizes.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }


            if (e.CommandName == "DeletePrizes")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdPrizes.DataKeys[index].Values["id"].ToString();

 
            }
        }

        protected void grdPrizes_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void btnAddNewPrizes_Click(object sender, EventArgs e)
        {
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());

        }


        //protected void grdPrizes_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        //{

        //    try
        //    {
        //        if (e.CommandName == RadGrid.InitInsertCommandName)
        //        {

        //            Guid strId = Guid.Empty;               
        //            Response.Redirect("edit.aspx?id=" + strId.ToString()  );


        //        }
        //        if (e.CommandName == "Edit")
        //        {
        //            GridDataItem item = e.Item as GridDataItem;
        //            Guid strId = new Guid(item.GetDataKeyValue("id").ToString());
                     
        //            Response.Redirect("edit.aspx?id=" + strId.ToString() );
        //        }
        //        if (e.CommandName == RadGrid.InitInsertCommandName)
        //        {
        //            Response.Redirect("edit.aspx");

        //        }


        //        if (e.CommandName == "Delete")
        //        {
        //            GridDataItem item = e.Item as GridDataItem;
        //            Guid strId = new Guid(item.GetDataKeyValue("id").ToString());

        //            prize _prize = _dal.GetPrizesBYID(strId);
        //            _dal.SkyLarkArenaEntities.Attach(_prize);
        //            _dal.DeleteObject(_prize);
        //            _dal.SaveChanges();

        //            grdPrizes.Rebind();
        //            grdPrizes.DataBind();

        //        }
        //    }



        //    catch (Exception ex)
        //    {


        //    }
        //}
    }
}