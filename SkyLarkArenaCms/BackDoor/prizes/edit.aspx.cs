﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using log4net;
using System.Data;
using System.Data.Entity;
namespace SkyLarkArenaCms.BackDoor.prizes
{
    public partial class edit : System.Web.UI.Page
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Global));
  
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        Guid _prizesID;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Prizes Edit";


            prize _prizes = new prize();

            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


            _prizes = _dal.GetPrizesBYID(_guid);


            if (!Page.IsPostBack)
            {

                txtPrizeName.Text = _prizes.prizeName;
                txtFoundationPayout.Text = _prizes.foundation.ToString();
                txtKidsPayout.Text = _prizes.kids.ToString();
                txtpointsRequired.Text = _prizes.points.ToString();
                txtTotalPayout.Text = _prizes.totalpayout.ToString();
                txtGifDescription.InnerText = _prizes.gift.ToString();
             

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
          

                
                string id = Request.QueryString["id"];
                if (id != "00000000-0000-0000-0000-000000000000")
                {
                    _prizesID = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

                }
 

                prize _prize = new prize();
                _prize = _dal.GetPrizesBYID(_prizesID);
                _prize.prizeName = txtPrizeName.Text;
                _prize.points = Convert.ToInt32(txtpointsRequired.Text);
                _prize.kids =Convert.ToDecimal(txtKidsPayout.Text);
                _prize.authUserName = User.Identity.Name.ToString();
                _prize.authCreatedDate = DateTime.Now;
                _prize.gift = txtGifDescription.InnerText;

                if (id == "00000000-0000-0000-0000-000000000000")
                {
                    _dal.SkyLarkArenaEntities.prizes.Add(_prize);
                }
                _dal.SkyLarkArenaEntities.SaveChanges();

                Response.Redirect("default.aspx");



          



        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}
     
 