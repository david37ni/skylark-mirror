﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" validateRequest="false" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.prizes.edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">

    <style>
   #btnCancel,
#btnSave{
    display: inline-block;
    vertical-align: top;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="box-body">

      
  <div class="form-group">
                      <label for="exampleInputEmail1">Prize Name</label>
          
  </div>


         
          <div class="form-group">
                      <label for="exampleInputEmail1">Prize Image Required</label>
              <asp:FileUpload ID="prizeImage" runat="server" />
   
                  
          </div>

 

     
      <div class="form-group">
                      <label for="exampleInputEmail1">Prize Name</label>
       
                   <asp:TextBox ID="txtPrizeName" CssClass="form-control" runat="server"></asp:TextBox>
   
                  
          </div>



          <div class="form-group">
                      <label for="exampleInputEmail1">Points Required</label>
       
                   <asp:TextBox ID="txtpointsRequired" CssClass="form-control" runat="server"></asp:TextBox>
   
                  
          </div>

             <div class="form-group">
                      <label for="exampleInputEmail1">Foundation Payout</label>
       
                   <asp:TextBox ID="txtFoundationPayout" CssClass="form-control" runat="server"></asp:TextBox>
   
                  
          </div>
             <div class="form-group">
                      <label for="exampleInputEmail1">Kids Payout</label>
       
                   <asp:TextBox ID="txtKidsPayout" CssClass="form-control" runat="server"></asp:TextBox>
   
                  
          </div>

               <div class="form-group">
                      <label for="exampleInputEmail1">Total Payout</label>
       
                   <asp:TextBox ID="txtTotalPayout" CssClass="form-control" runat="server"></asp:TextBox>
   
                  
          </div>

                 <div class="form-group">
                     <label for="exampleInputEmail1">Gift Description</label>
          
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    
                    <textarea id="txtGifDescription" runat="server" name="txtDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                 
                </div>
              </div>
              </div>
          </div>


          <div class="form-group">
                      <label for="exampleInputEmail1">Gift Image Upload</label>
     
                  
          </div>

            <div class="form-group">
                      <label for="exampleInputEmail1">Active</label>
                <asp:CheckBox ID="chkIsActive" runat="server" />
              
   
                  
          </div>
         
          
                 <div >
               <asp:Button ID="btnSave"  CssClass="btn btn-info btn-beside " runat="server" Text="Save" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-info btn-beside " OnClick="btnCancel_Click" Text="Cancel"  />
              
                 </div>
         
         </div>

</asp:Content>
