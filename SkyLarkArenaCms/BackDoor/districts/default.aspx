﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.districts._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
       <asp:Button ID="btnAddNewDistrict" runat="server" CssClass="btn btn-info btn-lg" Text="Add New District" OnClick="btnAddNewDistrict_Click" />
    
     <asp:ScriptManager ID="sptgrdSoccerPlayers" runat="server"></asp:ScriptManager>
    <asp:GridView ID="grdDistricts"  CssClass= "table table-striped table-bordered table-condensed" runat="server" DataKeyNames="id" AutoGenerateColumns="False" OnRowCommand="grdDistricts_RowCommand">
        <EmptyDataTemplate>

                  <div class="alert alert-info alert">
                      <h4><i class="icon fa fa-info"></i> Alert!</h4>
                    Their are no districts in the system. No User has registered as a player.
                  </div>
        </EmptyDataTemplate>
        <Columns>
             <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" Visible="false" SortExpression="id" />
            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name" />
            <asp:BoundField DataField="prefix" HeaderText="Prefix" ReadOnly="True" SortExpression="prefix" />
            <asp:BoundField DataField="siteId" HeaderText="siteId" ReadOnly="True" Visible="false"  SortExpression="siteId" />
             <asp:ButtonField CommandName="Edit" Text="Edit" />
        <asp:ButtonField CommandName="DeleteDistrict" Text="Delete" />
        </Columns>
    </asp:GridView>
       
        <br />
    
</asp:Content>
