﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SkyLarkArenaDal;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.districts
{
    public partial class _default : BasePage
    {
        district _district = new district();
        SkyLarkArenaDa _dal;
        protected void Page_Load(object sender, EventArgs e)
        {
            _dal = GetDataAccessLayer();
          

              MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Districts List";


            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;

             grdDistricts.DataSource = _dal.getAllDistricts(UserID);
            grdDistricts.DataBind();
        }

        protected void grdDistricts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdDistricts.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }


            if (e.CommandName == "DeleteDistrict")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdDistricts.DataKeys[index].Values["id"].ToString();


                       _district = _dal.GetDistrictsById(new Guid(value));
                       _district.isDeleted = true;
                       _district.deletedDate = DateTime.Now;
                       _dal.SkyLarkArenaEntities.SaveChanges();

                      Response.Redirect("default.aspx");

            }

        }

        protected void btnAddNewDistrict_Click(object sender, EventArgs e)
        {
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
        }

       
    }
}