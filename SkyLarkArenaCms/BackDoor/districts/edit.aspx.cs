﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using System.Data;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using System.Data.Entity;
namespace SkyLarkArenaCms.BackDoor.districts
{
    public partial class edit : System.Web.UI.Page
    {

        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        Guid _guid;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
                if (m != null) m.themeTitle = "Districts Edit";

                district _district = new district();


                if (!IsPostBack)
                {

                    string id = Request.QueryString["id"];
                    if (id != "00000000-0000-0000-0000-000000000000")
                    {
                        _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);
                        _district = _dal.GetDistrictsById(_guid);



                    }





                    txtName.Text = _district.name;
                    txtPrefix.Text = Convert.ToString(_district.prefix);
                }




            }
                catch(Exception ex)
            {
 

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

       
                district _district = new district();


                string id = Request.QueryString["id"];
                if (id != "00000000-0000-0000-0000-000000000000")
                {
                     _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

                }


                    _district = _dal.GetDistrictsById(_guid);


                    _district.name = txtName.Text;
                    _district.prefix = Convert.ToInt32(txtPrefix.Text);

                    _district.isDeleted = false;
                    if (id == "00000000-0000-0000-0000-000000000000")
                    {
                        _dal.SkyLarkArenaEntities.districts.Add(_district);
                    }


                    _dal.SkyLarkArenaEntities.SaveChanges();
            

                    Response.Redirect("default.aspx");

               
            
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}