﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master"  AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.systememails._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
 
    <asp:GridView ID="grdSystemEmails" runat="server"   CssClass= "table table-striped table-bordered table-condensed" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="entyEmails" OnRowCommand="grdSystemEmails_RowCommand">
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
            <asp:BoundField DataField="subject" HeaderText="subject" SortExpression="subject" />
            <asp:BoundField DataField="message" HeaderText="message" SortExpression="message" />
            <asp:BoundField DataField="authUserName" HeaderText="authUserName" SortExpression="authUserName" />
            <asp:BoundField DataField="authCreatedDate" DataFormatString="{0:dd/M/yyyy}" HeaderText="authCreatedDate" SortExpression="authCreatedDate" />
            <asp:BoundField DataField="emailDescription" HeaderText="emailDescription" SortExpression="emailDescription" />
            <asp:BoundField DataField="emailCode" HeaderText="emailCode" SortExpression="emailCode" />
            <asp:CheckBoxField DataField="isHtml" HeaderText="isHtml" SortExpression="isHtml" />
            <asp:CheckBoxField DataField="isActive" HeaderText="isActive" SortExpression="isActive" />
            <asp:BoundField DataField="emailType" HeaderText="emailType" SortExpression="emailType" />
            <asp:ButtonField CommandName="Edit" HeaderText="Edit" Text="Edit" />
           </Columns>
    </asp:GridView>
    
    <asp:EntityDataSource ID="entyEmails"  Include="emailTypes" runat="server" ConnectionString="name=UniteCmsEntities" DefaultContainerName="UniteCmsEntities" EnableFlattening="False" EntitySetName="systemEmails" ></asp:EntityDataSource>

</asp:Content>
