﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.systememails.edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

      <div class="box-body">
     

            <div class="form-group">
                      <label for="exampleInputEmail1">Email Type</label><br />
                    

                <asp:DropDownList ID="dpEmailTypes" runat="server"></asp:DropDownList>
   
          </div> 
           <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
       
                   <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
   
          </div>


             <div class="form-group">
                      <label for="exampleInputEmail1">Email Subject</label>
       
                   <asp:TextBox ID="txtSubject" CssClass="form-control" runat="server"></asp:TextBox>
   
          </div> 

               
         <div class="form-group">
                     <label for="exampleInputEmail1">Email Description</label>
          
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    
                    <textarea id="txtEmailDescription" runat="server" name="txtEmailDescription" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                 
                </div>
              </div>
              </div>
          

              
                   <div class="form-group">
                     <label for="exampleInputEmail1">Email Message</label>
          
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                    
                    <textarea id="txtEmailMessage" runat="server" name="txtEmailMessage" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                 
                </div>
              </div>
              </div>
         

              
          
        
           <div class="form-group">
               <asp:Button ID="btnSave" runat="server"  CssClass="btn btn-info"  Text="Save"  OnClick="btnSave_Click"/>
                 <asp:Button ID="btnCancel"  CssClass="btn btn-info" runat="server" Text="Save"  />
              
               </div>
          
                    </div>

</asp:Content>
