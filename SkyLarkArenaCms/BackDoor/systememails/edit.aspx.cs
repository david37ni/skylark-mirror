﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;

namespace SkyLarkArenaCms.BackDoor.systememails
{
    public partial class edit : System.Web.UI.Page
    {
        Guid _emailId;
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();

        protected void Page_Load(object sender, EventArgs e)
        {

            string id = Request.QueryString["id"];
            if (id != "00000000-0000-0000-0000-000000000000")
            {
                _emailId = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);
                dpEmailTypes.Enabled = false;
            }
            
            systemEmail _emails = new systemEmail();



            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


            _emails = _dal.GetSystemEmails(_guid);


            if (!Page.IsPostBack)
            {
                txtName.Text = _emails.name;
                txtSubject.Text = _emails.subject;
                dpEmailTypes.SelectedValue = _emails.emailType.ToString();
                txtEmailDescription.InnerText = _emails.emailDescription;
                txtEmailMessage.InnerText = _emails.message;                

            }


            var dpEmailTypessource = _dal.getAllEmailTypes();
            dpEmailTypes.DataSource = dpEmailTypessource;
            dpEmailTypes.DataValueField = "id";
            dpEmailTypes.DataTextField = "description";
            dpEmailTypes.DataBind();

            dpEmailTypes.Items.Add(new ListItem("Please Select", "0", true));
          

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                Guid _emailId;
                string id = Request.QueryString["id"];
                _emailId = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


                systemEmail _emails = new systemEmail();






                _emails = _dal.GetSystemEmails(_emailId);

                _emails.subject = txtSubject.Text;
                 _emails.authUserName = User.Identity.Name;
                _emails.emailType = new Guid(dpEmailTypes.SelectedValue);


                if (id == "00000000-0000-0000-0000-000000000000")
                {
                    _emails.authCreatedDate = DateTime.Now;

                    _dal.SkyLarkArenaEntities.systemEmails.Add(_emails);
                }



                _dal.SkyLarkArenaEntities.SaveChanges();
            }

            catch (Exception ex)
            {


                throw new EntityContextException("btnSave_Click failed.", ex);

            }

            Response.Redirect("default.aspx?teamId=00000000-0000-0000-0000-000000000000&id=00000000-0000-0000-0000-000000000000");

        }
    }
}