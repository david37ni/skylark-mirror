﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaCms.BackDoor.Themes.fluid;
using SkyLarkArenaCms.Helpers;

namespace SkyLarkArenaCms.BackDoor.systememails
{
    public partial class _default :BasePage
    {
        SkyLarkArenaDa _dal;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "System Emails";
            _dal = GetDataAccessLayer();
        }

        protected void grdSystemEmails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdSystemEmails.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());
            }
        }

       

        //protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        //{
        //    try
        //    {

        //        if (e.CommandName == RadGrid.InitInsertCommandName)
        //        {

        //            Guid strId = Guid.Empty;
        //            Response.Redirect("edit.aspx?id=" + strId.ToString() + "&teamId=" + Guid.Empty.ToString());


        //        }
        //        if (e.CommandName == "Edit")
        //        {
        //            GridDataItem item = e.Item as GridDataItem;
        //            Guid strId = new Guid(item.GetDataKeyValue("id").ToString());

        //            Response.Redirect("edit.aspx?id=" + strId.ToString() + "&teamId=" + Guid.Empty.ToString());
        //        }
        //        if (e.CommandName == RadGrid.InitInsertCommandName)
        //        {
        //            Response.Redirect("edit.aspx");

        //        }
        //        if (e.CommandName == "Delete")
        //        {
        //            GridDataItem item = e.Item as GridDataItem;
        //            Guid strId = new Guid(item.GetDataKeyValue("id").ToString());

        //            systemEmail _systememails = _dal.GetSystemEmails(strId);
        //            _dal.SkyLarkArenaEntities.Attach(_systememails);
        //            _dal.DeleteObject(_systememails);
        //            _dal.SaveChanges();

        //            grdSystemEmails.Rebind();
        //            grdSystemEmails.DataBind();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write(ex.ToString());

        //    }
        //}
    }
}