﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SkyLarkArenaCms.BackDoor.Themes.fluid;
using SkyLarkArenaDal;
namespace SkyLarkArenaCms.BackDoor.leagues
{
    public partial class _default : System.Web.UI.Page
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Global));
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
    
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPagePropertiesInterface m = Master as MasterPagePropertiesInterface;
            if (m != null) m.themeTitle = "Leagues List";


      
        }

     

        protected void grdLeagues_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteLeagues")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdLeagues.DataKeys[index].Values["id"].ToString();



                

                Leauge _league = _dal.GetLeaguesByLeagueId(new Guid(value));
                _league.isDeleted=true;
                _league.deletedDate =DateTime.Now;
                
                _dal.SaveChanges();
                 
            }
            if (e.CommandName == "Edit")
            {
                
                int index = Convert.ToInt32(e.CommandArgument);
                string value = this.grdLeagues.DataKeys[index].Values["id"].ToString();


                Response.Redirect("edit.aspx?id=" + value.ToString());

            }
            
        }

        protected void btnaddNewLeauge_Click(object sender, EventArgs e)
        {
            Response.Redirect("edit.aspx?id=" + Guid.Empty.ToString());
        }
    }
}