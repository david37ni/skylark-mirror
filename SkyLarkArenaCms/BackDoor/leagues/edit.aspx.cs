﻿using SkyLarkArenaDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyLarkArenaCms.BackDoor.leagues
{
    public partial class edit : System.Web.UI.Page
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Global));
        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        Guid _leaugeId;
      
        protected void Page_Load(object sender, EventArgs e)
        {

            
             

            Leauge _Leauge = new Leauge();

            string id = Request.QueryString["id"];

            Guid _guid = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);


            _Leauge = _dal.GetLeaguesByLeagueId(_guid);


            if (!Page.IsPostBack)
            {
                txtName.Text = _Leauge.LeagueName;
                txtLeagueId.Text = _Leauge.LeagueID.ToString();


            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                string id = Request.QueryString["id"];
                if (id != "00000000-0000-0000-0000-000000000000")
                {
                    _leaugeId = string.IsNullOrEmpty(id) ? new Guid() : new Guid(id);

                }


                Leauge _leauge = new Leauge();
                _leauge = _dal.GetLeaguesByLeagueId(_leaugeId);

                _leauge.LeagueName = txtName.Text;
                _leauge.LeagueID = Convert.ToInt32(txtLeagueId.Text);



               

                if (id == "00000000-0000-0000-0000-000000000000")
                {


                    _dal.SkyLarkArenaEntities.Leauges.Add(_leauge);

                }

                _dal.SkyLarkArenaEntities.SaveChanges();

                Response.Redirect("default.aspx");



            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Prizes Edit Exception {0}", ex.ToString()));

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
    }
}