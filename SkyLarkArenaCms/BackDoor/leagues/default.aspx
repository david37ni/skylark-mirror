﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BackDoor/Themes/fluid/Blank.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SkyLarkArenaCms.BackDoor.leagues._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cptUniteCmsHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cptUniteCmsContent" runat="server">

      
    
          <asp:Button ID="btnaddNewLeauge" runat="server" CssClass="btn btn-info btn-lg" Text="Add New League" OnClick="btnaddNewLeauge_Click" />
    
    <asp:EntityDataSource ID="entyLeagues" runat="server" ConnectionString="name=UniteCmsEntities" DefaultContainerName="UniteCmsEntities" EnableFlattening="False" EntitySetName="Leauges" Select="it.[id], it.[LeagueName], it.[teamId], it.[districtId], it.[siteId], it.[isactivated], it.[authUserName], it.[authCreatedDate], it.[LeagueID]"></asp:EntityDataSource>
    <asp:GridView ID="grdLeagues" runat="server" CssClass= "table table-striped table-bordered table-condensed" DataKeyNames="id"  AutoGenerateColumns="False" DataSourceID="entyLeagues" OnRowCommand="grdLeagues_RowCommand">
        <Columns>
      
            <asp:BoundField DataField="id"  Visible="false" HeaderText="id" ReadOnly="True" SortExpression="id" />


            <asp:BoundField DataField="LeagueName" HeaderText="League Name" ReadOnly="True" SortExpression="LeagueName" />
                                          <asp:BoundField DataField="LeagueID" HeaderText="LeagueID" ReadOnly="True" SortExpression="LeagueID" />
            <asp:BoundField DataField="teamId" HeaderText="Team" ReadOnly="True" SortExpression="teamId" />
            <asp:BoundField DataField="districtId" HeaderText="District" ReadOnly="True" SortExpression="districtId" />            
            <asp:BoundField DataField="authCreatedDate" DataFormatString="{0:dd/M/yyyy}"  HeaderText="Date" ReadOnly="True" SortExpression="authCreatedDate" />
            <asp:CheckBoxField DataField="isactivated" HeaderText="Activated" ReadOnly="True" SortExpression="isactivated" />
            
            
            <asp:ButtonField CommandName="Edit" Text="Edit" />
            <asp:ButtonField CommandName="DeleteLeagues" HeaderText="Delete" Text="Delete" />
        </Columns>
    </asp:GridView>
 
     
        <br />
        
 
</asp:Content>
