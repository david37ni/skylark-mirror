﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyLarkArenaCms.BackDoor.Themes.fluid
{
    public partial class signout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the user (though the method below is probably incorrect)
            // The basic idea is to get the user record using a user key
            // stored in the session (such as the user id).
          
            // Ensure user is valid

            FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
            _identity = (FormsIdentity)Context.User.Identity;
          
            if (!_identity.IsAuthenticated)
            {
                HttpContext.Current.Session.Abandon();
                FormsAuthentication.SignOut();
                HttpContext.Current.Response.Redirect("~/Login.aspx");
            }
        }
    }
}