﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using SkyLarkArenaDal.Models;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;

namespace SkyLarkArenaCms.BackDoor.Themes.fluid
{
    public partial class mailbox : MasterBasePage
    {
        SkyLarkArenaDa _dal;
   
        protected void Page_Load(object sender, EventArgs e)
        {
            _dal = GetDataAccessLayer();
            if (Context.User.Identity.AuthenticationType == "Forms" && Context.User.Identity.IsAuthenticated)
            {
                string userName;
                FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
                _identity = (FormsIdentity)Context.User.Identity;
                Guid _UserId = new Guid(Session["userId"].ToString());

                lblCount.Text = _dal.GetUserMessageCount(_UserId).ToString();
                lblminiCount.Text = lblCount.Text;
                userName = _identity.Name;
                lblhiddenName.Text = userName.ToString();
                lblFullName.Text = userName.ToString();



            }
            else
            {
                Response.Redirect("~/BackDoor/Login.aspx");

            }
        }
    }
}