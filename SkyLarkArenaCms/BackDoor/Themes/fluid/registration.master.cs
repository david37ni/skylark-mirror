﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;

namespace SkyLarkArenaCms.BackDoor.Themes.fluid
{
    public partial class registration : System.Web.UI.MasterPage
    {

        SkyLarkArenaDa _dal = new SkyLarkArenaDa();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                  versionNumber.InnerText = Constants.appVersion;

                if (!IsPostBack)
                {
                    var ddlLeagues = _dal.GetLeagues();
                  
                        rdLeagues.DataSource = ddlLeagues;
                        rdLeagues.DataValueField = "LookupValue";
                        rdLeagues.DataTextField = "LookupDescription";
                        rdLeagues.DataBind();
                    
                }

                  
            }catch(Exception ex)
            {
                

            }



        }

        protected void btnRegsiter_Click(object sender, EventArgs e)
        {

            try
            {
                bool checkUserName = _dal.CheckIfUserNameExists(txtUserName.Text);

                if (checkUserName == true)
                {
                    userNameExists.Visible = true;
                }
                else
                    userNameExists.Visible = false;

                if (txtEmail.Text != "")
                {

                    bool checkEmail = _dal.CheckIfEmailExists(txtEmail.Text);
                    if (checkEmail == true)
                    {
                        emailExists.Visible = true;
                        emailExistslink.Visible = true;

                    }
                    else
                    {
                        emailExists.Visible = false;
                        emailExistslink.Visible = false;

                    }



                    if (checkUserName == false && checkEmail == false)//if it is a valid username and email address let them register.
                    {
                        Guid id = _dal.ReigsterUser(txtFirstName.Text, txtLastName.Text, txtUserName.Text, txtPassword.Text, txtEmail.Text, new Guid(rbaccoutnType.SelectedValue), false);
                        userNameExists.Visible = false;

                        emailExists.Visible = false;
                        emailExistslink.Visible = false;
                        _dal.SendEmail(txtUserName.Text, "test Message", txtEmail.Text);


                    }
                }


            }

            catch (Exception ex)
            {


            }
        }

        protected void txtEmail_TextChanged(object sender, EventArgs e)
        {




        }

 
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/BackDoor/Login.aspx");
        }

        protected void rbaccoutnType_SelectedIndexChanged1(object sender, EventArgs e)
        {

            rdLeagues.Visible = true;
            if (rbaccoutnType.SelectedValue == Constants.teamGuid.ToString())
            {
                rdLeagues.Visible = true;
            }
         
        }
    }
}