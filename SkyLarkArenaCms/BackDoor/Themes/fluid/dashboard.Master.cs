﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.Themes.fluid
{
    public partial class dashboard : MasterBasePage
    {

        SkyLarkArenaDa _dal;
        protected void Page_Load(object sender, EventArgs e)
        {


            _dal = GetDataAccessLayer();
            int nonActiatedUsers = _dal.GetRegisteredUsersCount();
            Guid _UserId = new Guid(Session["userId"].ToString());

            lblCount.Text = _dal.GetUserMessageCount(_UserId).ToString();
            lblminiCount.Text = lblCount.Text;
        

            lblplayersJoined.Text = nonActiatedUsers.ToString();
            lblacticvations.Text = nonActiatedUsers.ToString();

            int notProceesedPointsPlayer = _dal.GetPointsToBeProcessedCount();

            lblPointsToReDeem.Text = notProceesedPointsPlayer.ToString();

            if (Context.User.Identity.AuthenticationType == "Forms" && Context.User.Identity.IsAuthenticated)
            {
                FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
                  _identity = (FormsIdentity)Context.User.Identity;

               _identity = (FormsIdentity)Context.User.Identity;


               lblFullName.Text = _identity.Name;
                lblhiddenName.Text =  _identity.Name;


                 

            }else
            {
                Response.Redirect("~/BackDoor/Login.aspx");

            }
        }

        protected void btnSignoOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
                                
            if (Response.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
            }
        }
    }
}