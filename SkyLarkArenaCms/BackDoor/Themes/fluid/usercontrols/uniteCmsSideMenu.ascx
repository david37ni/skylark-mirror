﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uniteCmsSideMenu.ascx.cs" Inherits="SkyLarkArenaCms.BackDoor.Themes.fluid.usercontrols.uniteCmsSideMenu1" %>
      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
            <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><asp:Label ID="litName" runat="server" Text=""></asp:Label></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
    
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
                   
              
             

                   <li class="treeview active">
              <a href="~/BackDoor/messages/default.aspx"  runat="server">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li > <a href="~/BackDoor/messages/default.aspx"  runat="server">Inbox <span class="label label-primary pull-right">
                    <asp:Label ID="lblcount" runat="server" Text=""></asp:Label></span></a></li>
                <li><a href="compose.html">Compose</a></li>
                <li><a href="read-mail.html">Read</a></li>
              </ul>
            </li>
                 <li>
              <a href="~/BackDoor/channel/default.aspx" runat="server">
                <i class="fa fa-users" ></i> <span>Channels </span> <span class="label label-primary pull-right"> New</span>
              </a>
            </li>


                 <li>
              <a href="~/BackDoor/games/default.aspx" runat="server">
                <i class="fa fa-users" ></i> <span>Games </span> <span class="label label-primary pull-right"> Updated</span>
              </a>
            </li>

              
                  <li>
              <a href="~/BackDoor/players/default.aspx?id=00000000-0000-0000-0000-000000000000&teamId=00000000-0000-0000-0000-000000000000" runat="server">
                <i class="fa fa-user"></i> <span>Players </span>   <span class="label label-primary pull-right">3 New</span>
              </a>
            </li>
                  <li>
              <a href="~/BackDoor/teams/default.aspx" runat="server">
                <i class="fa fa-users" ></i> <span>Teams </span> 
              </a>
            </li>

                     <li>
              <a href="~/BackDoor/tournaments/default.aspx" runat="server">
                <i class="fa fa-users" ></i> <span>Tournaments </span> 
              </a>
            </li>
               
               
                    <li>
              <a href="~/BackDoor/districts/default.aspx" runat="server">
                <i class="fa fa-area-chart"></i> <span>Districts </span> 
              </a>
            </li>

                                <li>
              <a href="~/BackDoor/leagues/default.aspx" runat="server">
                <i class="fa fa-area-chart"></i> <span>Leagues </span> <small class="label pull-right bg-blue">New</small>
              </a>
            </li>

             
                          <li>
              <a href="~/BackDoor/playerpoints/default.aspx" runat="server">
                <i class="fa fa-area-chart"></i> <span>Player Points </span> 
              </a>
            </li>

                             <li>
              <a href="~/BackDoor/prizes/default.aspx" runat="server">
                <i class="fa fa-area-chart"></i> <span>Prizes </span> <small class="label pull-right bg-blue">New</small>
              </a>
            </li>

            

                        <li>
              <a href="~/BackDoor/systememails/default.aspx" runat="server">
                <i class="fa fa-envelope"></i> <span>System Emails </span> 
              </a>
            </li>
              
              
          
            
           </ul>
        </section>
        <!-- /.sidebar -->
      </aside>