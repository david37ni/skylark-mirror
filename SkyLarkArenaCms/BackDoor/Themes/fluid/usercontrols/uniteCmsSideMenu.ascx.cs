﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;

namespace SkyLarkArenaCms.BackDoor.Themes.fluid.usercontrols
{
    public partial class uniteCmsSideMenu1 : System.Web.UI.UserControl
    {
        public string userName { get; set; }
      
        protected void Page_Load(object sender, EventArgs e)
        {

            SkyLarkArenaDa _dal = new SkyLarkArenaDa();
   

            if (Context.User.Identity.AuthenticationType == "Forms" && Context.User.Identity.IsAuthenticated)
            {
                FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
                _identity = (FormsIdentity)Context.User.Identity;

                userName = _identity.Name;
                litName.Text = userName;

                Guid _UserId = new Guid(Session["userId"].ToString());

                lblcount.Text = _dal.GetUserMessageCount(_UserId).ToString();
        


            }

        }
    }
}