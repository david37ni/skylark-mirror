﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SkyLarkArenaDal;
using SkyLarkArenaDal.Models;
using System.Web.Security;
using SkyLarkArenaCms.Helpers;
namespace SkyLarkArenaCms.BackDoor.Themes.fluid
{
    public partial class Blank : MasterBasePage, MasterPagePropertiesInterface
    {

        public string themeTitle
        {
            get { return this.litPanelTitle.Text; }
            set { this.litPanelTitle.Text = value; }
        }
        public string userName;
        SkyLarkArenaDa _dal;
     
        protected void Page_Load(object sender, EventArgs e)
        {
            _dal = GetDataAccessLayer();
         

            Guid _UserId = new Guid(Session["userId"].ToString());

            lblCount.Text = _dal.GetUserMessageCount(_UserId).ToString();
            lblminiCount.Text = lblCount.Text;
        
            if (Context.User.Identity.AuthenticationType == "Forms" && Context.User.Identity.IsAuthenticated)
            {
                FormsIdentity _identity = (FormsIdentity)Context.User.Identity;
                  _identity = (FormsIdentity)Context.User.Identity;
         
               userName = _identity.Name;
               lblFullName.Text = userName;
               lblhiddenName.Text = userName;

               
                
               
            }
            else
            {
                Response.Redirect("~/BackDoor/Login.aspx");

            }


        }

        protected void signOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();

            if (Response.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
            }

        }
    }
}