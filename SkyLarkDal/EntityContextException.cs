﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal
{
    public class EntityContextException : Exception
    {
        public EntityContextException(string message)
            : base(message)
        {
        }

        public EntityContextException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
