﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal
{
   public  class Constants
    {

       public const string appTitle="Soccer Admin";
       public const string appVersion = "Version 1.0";


       //account type guids
       public static readonly Guid teamGuid = new Guid("5F24D996-87E0-46B8-B49A-88D03248AA5E");
       public static readonly Guid leagueGuid = new Guid("450FBDE4-BF82-4726-B038-B4F1CBFA60AF");
       public static readonly Guid playerGuid = new Guid("E60070CD-8C44-425D-A70F-18B3D6C44485");
       public static readonly Guid clubGuid = new Guid("77E92CC2-1756-4EB6-A9C9-329FA35DA11C");


       //super admin
       public static readonly Guid adminGuid = new Guid("195FBE4B-72E0-4ADD-82D7-A65E2A8CBD54");


       //enums

      public enum FriendStatus
       {
           Requested =0,
           Cancelled = 1,
           Rejected = 2,
           Blocked = 3
       };
       
    }
}
