//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SkyLarkArenaDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class notification
    {
        public System.Guid id { get; set; }
        public Nullable<System.Guid> userId { get; set; }
        public Nullable<System.Guid> type { get; set; }
        public string NotificationType { get; set; }
        public Nullable<System.Guid> NotificationTypeID { get; set; }
        public Nullable<System.Guid> recipientId { get; set; }
        public string url { get; set; }
        public Nullable<System.Guid> senderId { get; set; }
        public string message { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> authCreatedDate { get; set; }
        public string deletedDate { get; set; }
        public string authUserName { get; set; }
        public Nullable<System.Guid> authUserId { get; set; }
    }
}
