//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SkyLarkArenaDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class team
    {
        public System.Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<System.Guid> playerId { get; set; }
        public Nullable<System.Guid> districtId { get; set; }
        public Nullable<System.Guid> leagueId { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string author { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public string authUserName { get; set; }
        public string authdate { get; set; }
        public Nullable<System.Guid> siteId { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> deletedDate { get; set; }
        public Nullable<bool> isArchived { get; set; }
        public Nullable<System.DateTime> archivedDate { get; set; }
        public Nullable<System.Guid> userId { get; set; }
    }
}
