﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal.Interfaces
{
    interface ISoftDeletable
    {


        bool isDeleted { get; set; }
    }
}
