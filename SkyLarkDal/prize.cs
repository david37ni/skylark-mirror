//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SkyLarkArenaDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class prize
    {
        public System.Guid id { get; set; }
        public string prizeName { get; set; }
        public Nullable<int> points { get; set; }
        public Nullable<decimal> foundation { get; set; }
        public Nullable<decimal> kids { get; set; }
        public Nullable<decimal> totalpayout { get; set; }
        public string gift { get; set; }
        public string gifImage { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string authUserName { get; set; }
        public Nullable<System.DateTime> authCreatedDate { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> deletedDate { get; set; }
        public Nullable<System.Guid> userId { get; set; }
    }
}
