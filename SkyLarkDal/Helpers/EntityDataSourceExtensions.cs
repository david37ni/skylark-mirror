﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SkyLarkArenaDal.Helpers
{
   public  static class EntityDataSourceExtensions
    {

        public static TEntity GetItemObject<TEntity>(object dataItem)

        where TEntity : class
        {

            var entity = dataItem as TEntity;

            if (entity != null)
            {

                return entity;

            }

            var td = dataItem as ICustomTypeDescriptor;

            if (td != null)
            {

                return (TEntity)td.GetPropertyOwner(null);

            }

            return null;

        
        }
        public static bool IsNullOrEmpty(this Guid guid)
        {
            return (guid == Guid.Empty);
        }
    }
}
