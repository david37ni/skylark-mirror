//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SkyLarkArenaDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class report
    {
        public System.Guid id { get; set; }
        public Nullable<System.Guid> reportieUserId { get; set; }
        public Nullable<System.Guid> userReported { get; set; }
        public string itemReported { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> authCreateDate { get; set; }
        public string authUserName { get; set; }
        public Nullable<System.DateTime> deletedDate { get; set; }
        public string ipAddressUser { get; set; }
        public string ipAddressReportie { get; set; }
    }
}
