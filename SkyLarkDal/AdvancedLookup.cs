﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal
{
   public class AdvancedLookup
    {

       public AdvancedLookup(Guid lookupValue, string lookupDescription)
        {
            LookupValue = lookupValue;
            LookupDescription = lookupDescription;
        }

        public Guid  LookupValue { get; set; }
        public string LookupDescription { get; set; }

    }
}
