﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyLarkArenaDal.Models;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Linq;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Xml.Linq;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Data.Entity;
using SkyLarkArenaDal;

namespace SkyLarkArenaDal
{
    public class SkyLarkArenaDa : ContextBase
    {
        public SkyLarkArenaEntities _dal = new SkyLarkArenaEntities();
        
        public string userName { get; set; }
        public bool isAdmin { get; set; }
        public bool isPlayer { get; set; }
        public bool canView { get; set; }
      

       
        public string FullName { get; set; }
        public bool canEdit { get; set; }
        public bool canDelete { get; set; }


        //email enums
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SkyLarkArenaEntities));

 public Guid GetUserId(string UserName)
        {

            string myScalarQuery = "select UserId from aspnet_Users where UserName = '" + UserName + "'";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            Guid _userId = (Guid)myCommand.ExecuteScalar();
            myConnection.Close();

            return _userId;
        }
 public int GetUserMessageCount(Guid _userId)
        {

            string myScalarQuery = "select count(*) from messages where userId = '" +_userId.ToString() + "'";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int)myCommand.ExecuteScalar();
            myConnection.Close();

            return count;
        }

        public int  GetRegisteredUsersCount()
        {

            string myScalarQuery = "select count(*) from aspnet_Users where isActivated = 0";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());
 
            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int) myCommand.ExecuteScalar();
            myConnection.Close();

            return count;
        }
        public int GetPointsToBeProcessedCount()
        {

            string myScalarQuery = "select count(*) from player_points where isProcessed = 0";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int)myCommand.ExecuteScalar();
            myConnection.Close();

            return count;
        }


        public int GetTotalPointsToAdd(Guid _userId)
        {


            string myScalarQuery = "select sum(points) from player_points where playerId = '" + _userId.ToString() + "' and isProcessed=0 and isDeleted=0";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int)myCommand.ExecuteScalar();
            myConnection.Close();

            return count;


        }

        public int GetPlayerCountByTeamId(Guid teamid)
        {

            string myScalarQuery = "select count(*) from player where teamId = '" + teamid.ToString() +"'";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int)myCommand.ExecuteScalar();
            myConnection.Close();

            return count;
        }

        public string genPlayerID(Guid districtId,Guid leauegId)
        {


            try
            {
                string myScalarQuery = "select LeagueID from Leauges where id = '" + leauegId.ToString() + "'";
                string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
                SqlConnection myConnection = new SqlConnection(cs.ToString());

                SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
                myCommand.Connection.Open();
                int leagueNumber = (int)myCommand.ExecuteScalar();


                string myScalarQuerydistrict = "select prefix from district where id = '" + districtId.ToString() + "'";
               SqlConnection myConnectiondistrict = new SqlConnection(cs.ToString());

                SqlCommand myCommanddistrict = new SqlCommand(myScalarQuerydistrict, myConnectiondistrict);
                myCommanddistrict.Connection.Open();
                int districtNumber = (int)myCommanddistrict.ExecuteScalar();


                return GetNextPlayerId(districtNumber, leagueNumber);
            }
            catch (Exception ex)
            {
                throw new EntityContextException("genPlayerID failed.", ex);
            }
        

        
        }

      
        public bool CheckIfEmailExists(string email)
        {

            string myScalarQuery = "select count(*) from aspnet_Users where emailAddress = '" + email.ToString() + "'";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());

            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int)myCommand.ExecuteScalar();
            myConnection.Close();

            if (count >= 1)
                return true; // username exists
            else
                return false;
        }
        public bool CheckIfUserNameExists(string UserName)
     
           {

               string myScalarQuery = "select count(*) from aspnet_Users where UserName = '" + UserName.ToString() + "'";
            string cs = ConfigurationManager.ConnectionStrings["uniteCms"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cs.ToString());
 
            SqlCommand myCommand = new SqlCommand(myScalarQuery, myConnection);
            myCommand.Connection.Open();
            int count = (int) myCommand.ExecuteScalar();
            myConnection.Close();

            if (count >= 1)
               return true; // username exists
            else
                return false;
        }


        public tournament GetTorunamentById(Guid _tournamentId)
        {

            try
            {
                if (_tournamentId == Guid.Empty)
                {
                    tournament _tournament = new tournament();
                    return _tournament;
                }
                else
                {

                    var q = SkyLarkArenaEntities.tournaments.Where(p => p.id == _tournamentId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Tournaments could not be found {0}!", _tournamentId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetTorunamentById failed.", ex);
            }
        }
 
        public district GetDistrictsById(Guid _districtId)
        {
            
          try
            {
                if (_districtId == Guid.Empty)
                {
                    district _district = new district();
                    return _district;
                }
                else
                {

                    var q = SkyLarkArenaEntities.districts.Where(p => p.id == _districtId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A district could not be found {0}!", _districtId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPlayerBYID failed.", ex);
            }
        }
        public player GetPlayerBYID(Guid _playerId)
        {
            try
            {
                if (_playerId == Guid.Empty)
                {
                    player _player = new player();                  
                    return _player;
                }
                else
                {
                    var q = SkyLarkArenaEntities.players.Where(p => p.id == _playerId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A player could not be found {0}!", _playerId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPlayerBYID failed.", ex);
            }
        }

        public player_points GetPlayerPointsBYID(Guid _playerId)
        {
            try
            {
                if (_playerId == Guid.Empty)
                {
                    player_points _player_points = new player_points();
                    return _player_points;
                }
                else
                {
                    var q = SkyLarkArenaEntities.player_points.Where(p => p.playerId == _playerId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A player points could not be found {0}!", _playerId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPlayerPointsBYID failed.", ex);
            }
        }
        public channel GetChannelsBYPlayerId(Guid _channelId)
        {
            try
            {
                if (_channelId == Guid.Empty)
                {
                    channel _channels = new channel();
                    return _channels;
                }
                else
                {
                    var q = SkyLarkArenaEntities.channels.Where(p => p.id == _channelId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Channel  could not be found {0}!", _channelId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPlayerPointsBYID failed.", ex);
            }
        }

        public game GetGamesById(Guid _gamesId)
        {
            try
            {
                if (_gamesId == Guid.Empty)
                {
                    game _game = new game();
                    return _game;
                }
                else
                {
                    var q = SkyLarkArenaEntities.games.Where(p => p.id == _gamesId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Channel  could not be found {0}!", _gamesId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetGamesById failed.", ex);
            }
        }
        public prize GetPrizesWithinPoints(int _points)
        {
            try
            {

                var q = SkyLarkArenaEntities.prizes.Where(p => p.points <= _points && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("No Prizes exists below user points {0}!", _points));
                    else
                        return q.ToList()[0];
                 
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPrizesWithinPoints failed.", ex);
            }
        }
        public prize GetPrizesBYID(Guid _prizeId)
        {
            try
            {
                if (_prizeId == Guid.Empty)
                {
                    prize _prize = new prize();
                    return _prize;
                }
                else
                {
                    var q = SkyLarkArenaEntities.prizes.Where(p => p.id == _prizeId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Prize could not be found {0}!", _prizeId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPrizesBYID failed.", ex);
            }
        }

        public Leauge GetLeaguesByLeagueId(Guid _leagueId)
        {
            try
            {
                if (_leagueId == Guid.Empty)
                {
                    Leauge _league = new Leauge();
                    return _league;
                }
                else
                {
                    var q = SkyLarkArenaEntities.Leauges.Where(p => p.id == _leagueId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Prize could not be found {0}!", _leagueId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetLeaguesByLeagueId failed.", ex);
            }
        }

        public player_points GetPointsById(Guid _ppoints)
        {
            try
            {
                if (_ppoints == Guid.Empty)
                {
                    player_points _points = new player_points();
                    return _points;
                }
                else
                {
                    var q = SkyLarkArenaEntities.player_points.Where(p => p.id == _ppoints && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A Point could not be found {0}!", _ppoints));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPointsById failed.", ex);
            }
        }

        public systemEmail GetSystemEmails(Guid _emailID)
        {
            try
            {
                if (_emailID == Guid.Empty)
                {
                    systemEmail _systemEmail = new systemEmail();
                    return _systemEmail;
                }
                else
                {
                    var q = SkyLarkArenaEntities.systemEmails.Where(p => p.id == _emailID && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("A system email could not be found {0}!", _emailID));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetSystemEmails failed.", ex);
            }
        }

        public player_points GetPointsTORedeemByPlayerId(Guid _pointsId)
        {
           try
             {
                 if (_pointsId == Guid.Empty)
                {
                    player_points _playerpoints = new player_points();
                    return _playerpoints;
                }
                else
                {
                    var q = SkyLarkArenaEntities.player_points.Where(p => p.id == _pointsId && p.isDeleted==false);

                    if (q == null)
                        throw new EntityContextException(string.Format("No Points could be found for player to be processed {0}!", _pointsId));
                    else
                        return q.ToList()[0];
                }
            }
           catch (Exception ex)
            {
                throw new EntityContextException("GetPointsTORedeemByPlayerId failed.", ex);
            }
        }




        public message GetMessagesById(Guid _messageId, Guid _userId)
        {
            try
            {
                if (_messageId == Guid.Empty)
                {
                    message _playerpoints = new message();
                    return _playerpoints;
                }
                else
                {
                    var q = SkyLarkArenaEntities.messages.Where(p => p.id == _messageId && p.userId == _userId && p.isDeleted == false);

                    if (q == null)
                        throw new EntityContextException(string.Format("No Message could be found for player to be processed {0}!", _messageId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetPointsTORedeemByPlayerId failed.", ex);
            }
        }
      


        public team GetTeamByTeamId(Guid  teamId)
             {
            try
            {
                if (teamId == Guid.Empty)
                {
                    team _team = new team();
                   
                    return _team;
                }
                else
                {
                    var q = SkyLarkArenaEntities.teams.Where(p => p.id == teamId);
                    
                    if (q == null)
                        throw new EntityContextException(string.Format("A team could not be found {0}!", teamId));
                    else
                        return q.ToList()[0];
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetTeamByTeamId failed.", ex);
            }
      



        }
 
        
        protected bool isPlayerdb(string userName)
        {

            try
            {


              Users adminUsers = (from users in SkyLarkArenaEntities.Users
                           where users.roleId == new Guid("75711058-F76B-4091-91DC-585D242E0773") && users.UserName == userName
                           select users).FirstOrDefault();

                if (adminUsers != null)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw new EntityContextException("isPlayer failed.", ex);
            }

        }

 
        protected bool isAdmindb(string userName)
        {

          try{
               Users adminUsers = (from users in SkyLarkArenaEntities.Users
                           where users.roleId == new Guid("ED85788D-72DA-4D0A-8D5E-B5378FC00592") && users.UserName == userName
                           select users).FirstOrDefault();
 
            if (adminUsers != null)
                return true;
            else
                return false;

           }
        catch (Exception ex)
        {
            throw new EntityContextException("isAdmin failed.", ex);
        }
      

        }
        public void AddToPlayers(player record)
        {
            try
            {
                SkyLarkArenaEntities.players.Add(record);
                
            }
            catch (Exception ex)
            {
                throw new EntityContextException("AddToPlayers failed.", ex);
            }
        
        
        }

        public Guid ReigsterUser(string firstName,string lastName,string userName,string password,string email,Guid accountType,bool sendEmail)
        {

            Users _myuser = new Users();

            _myuser.firstName = firstName;
            _myuser.lastName = lastName;
            _myuser.UserName = userName;
            _myuser.password = password;
            _myuser.emailAddress =email;
            _myuser.accountType = accountType;
            _myuser.isActivated = false;
            _dal.Users.Add(_myuser);
     
           _dal.SaveChanges();


           Guid id = _myuser.UserId;
           string message = "Thanks for registering with the fundrasing program. Please click here to activate your <a href='activateaccount.aspx?id='" + id + ">Activate</a>";


           SendEmail("Regestration FundRasing",message,email);


           return id;

        }


        public IQueryable<player> getAllPlayersByTeamId(Guid teamId)            
        {
            var _player = SkyLarkArenaEntities.players.Where(a => a.teamId == teamId && a.isDeleted==false).ToList();
            return _player.AsQueryable();
        }

  

    
        public IQueryable<player_points> getAllPlayersPointsByPlayerId(Guid _playerId)
        {
            var _player = SkyLarkArenaEntities.player_points.Where(a => a.playerId == _playerId && a.isProcessed==true && a.isActivated==true && a.isDeleted==false).ToList();
            return _player.AsQueryable();
        }

        public IQueryable<player> getAllPlayers(Guid _userId)
        {
            var _player = SkyLarkArenaEntities.players.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.createdDate).ToList();
            return _player.AsQueryable();
        }

        public IQueryable<team> getAllTeams(Guid _userId )       {
            var _teams = SkyLarkArenaEntities.teams.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authdate).ToList();
            return _teams.AsQueryable();
        }
        public IQueryable<player_points> getAllPoints()
        {
            var _playerPoints = SkyLarkArenaEntities.player_points.Where(f=> f.isDeleted==false).OrderByDescending(o => o.authCreatedDate).ToList();
            return _playerPoints.AsQueryable();
        }


        public IQueryable<prize> getAllprizesByPoints(int _points)
        {
            var _prize = SkyLarkArenaEntities.prizes.OrderByDescending(o=>o.authCreatedDate).Where(a=> a.points <= _points && a.isDeleted==false).ToList();
            return _prize.AsQueryable();
        }


        public IQueryable<emailType> getAllEmailTypes()
        {
            var _emailtypes = SkyLarkArenaEntities.emailTypes.Where(f=> f.isDeleted==false).OrderByDescending(o => o.authCreateDate).ToList();
            return _emailtypes.AsQueryable();
        }

        public IQueryable<message> getAllMessages(Guid  _userId)
        {
            var _message = SkyLarkArenaEntities.messages.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authCreatedDate).ToList();
            return _message.AsQueryable();
        }
        public IQueryable<channel> getAllChannels(Guid _userId)
        {
            var _channels = SkyLarkArenaEntities.channels.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authCreatedDate).ToList();
            return _channels.AsQueryable();
        }
        public IQueryable<game> getAllGames(Guid _userId)
        {
            var _games = SkyLarkArenaEntities.games.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authCreatedDate).ToList();
            return _games.AsQueryable();
        }

        public IQueryable<tournament> getAllTorunments(Guid _userId)
        {
            var torunments = SkyLarkArenaEntities.tournaments.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authCreatedDate).ToList();
            return torunments.AsQueryable();
        }

        public IQueryable<player_points> getAllPlayerPoints(Guid _userId)
        {
            
            var _player = SkyLarkArenaEntities.player_points.Where(f=> f.isDeleted==false).OrderByDescending(o => o.authCreatedDate ).ToList();
            return _player.AsQueryable();
        }
        public IQueryable<district> getAllDistricts(Guid _userId)
        {

            var _dsitricts = SkyLarkArenaEntities.districts.Where(f => f.isDeleted == false && f.userId == _userId).OrderByDescending(o => o.authCreatedDate).ToList();
            return _dsitricts.AsQueryable();
        }
      
        public void SendEmail(string UserName ,string message,string toEmailAddress)
        {
            try
            {

                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("davidbuckleyni@gmail.com", "Lisa2015d");

                MailMessage mm = new MailMessage("donotreply@domain.com", toEmailAddress.Trim(), "test", message);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);

            }
            catch (Exception ex)
            {
                throw new EntityContextException("SendEmail failed.", ex);
            }

        }
      
        public Users VerifyPassword(string userName, string password)
        {
            //The ".FirstOrDefault()" method will return either the first matched
            //result or null
            Users myUser = SkyLarkArenaEntities.Users
                .FirstOrDefault(u => u.UserName == userName
                             && u.password == password);


            
            if (myUser == null)    //User was not found
            {
           
               return null;
            }
            else    //User was  found
            {
                if (isPlayerdb(userName))
                    isPlayer = true;
                if (isAdmindb(userName))
                    isAdmin = true;


                FullName = myUser.firstName + " " + myUser.lastName;
                
                  userName = myUser.UserName.ToLower();
                return myUser;        
                //Do something to let them know that their credentials were not valid
            }
        }


        public string GetNextPlayerId(int districtId,int leagueId)
        {



            string _nextId = GetNextId("player").ToString();


            return districtId.ToString() +"-"+ leagueId.ToString() + "-" + _nextId.ToString().PadRight(10, '0');


        }
        
        public int GetNextId(string tableName)
        {
            int nextId = -1;

            try
            {
                tableName = tableName.ToUpper().Trim();

                var q = from n in SkyLarkArenaEntities.nextPlayerIds where n.tableName == tableName select n;

                if (q == null)
                    throw new EntityContextException(string.Format("The next ID number for {0} could not be retrieved!", tableName));

                nextPlayerId nextIdEntity = q.ToList()[0];

                nextId = Convert.ToInt32(nextIdEntity.nextNumber);
                nextIdEntity.nextNumber = nextId + 1;

                if (SkyLarkArenaEntities.SaveChanges()==   0)
                    throw new EntityContextException("The new NextIdNumber could not be saved!");
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetNextId failed.", ex);
            }

            return nextId;
        }
        //4672db3b-cd45-42ea-b66b-839f9a87c392 Gender Guid Id
        public List<StandardLookup> GetGenders()
        {
            List<StandardLookup> lookups = new List<StandardLookup>();

            try
            {
                var q = from lookup in SkyLarkArenaEntities.lookupValues.Where(a => a.typeID == new Guid("4672db3b-cd45-42ea-b66b-839f9a87c392"))
                        orderby lookup.lookupDescription
                        select new
                        {
                            LookLookupValue = lookup.lookupValue1,
                            LookupDescription = lookup.lookupDescription.Trim()
                        };

                if (q != null)
                {
                    Array.ForEach(q.ToArray(), l =>
                    {
                        lookups.Add(new StandardLookup(l.LookLookupValue, l.LookupDescription));
                    });
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetGenders failed.", ex);
            }

            return lookups;
        
        }

        //4672db3b-cd45-42ea-b66b-839f9a87c392 Gender Guid Id
        public List<AdvancedLookup> GetTeams()
        {
            List<AdvancedLookup> lookups = new List<AdvancedLookup>();

            try
            {
                var q = from lookup in SkyLarkArenaEntities.teams
                        orderby lookup.name
                        select new
                        {
                            LookLookupValue = lookup.id,
                            LookupDescription = lookup.name
                        };

                if (q != null)
                {
                    Array.ForEach(q.ToArray(), l =>
                    {
                        lookups.Add(new AdvancedLookup(l.LookLookupValue, l.LookupDescription));
                    });
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetTeams failed.", ex);
            }

            return lookups;
        }

    
        public List<DistricsList> GetDistricts()
        {
            List<DistricsList> lookups = new List<DistricsList>();

            try
            {
                var q = from lookup in SkyLarkArenaEntities.districts
                        orderby lookup.name
                        select new
                        {
                            Name = lookup.name,
                            Prefix = lookup.prefix
                        };

                if (q != null)
                {
                    Array.ForEach(q.ToArray(), D =>
                    {
                        lookups.Add(new DistricsList(D.Name, D.Prefix.Value));
                    });
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetDistricts failed.", ex);
            }

            return lookups;
        }

        //4672db3b-cd45-42ea-b66b-839f9a87c392 Gender Guid Id
        public List<AdvancedLookup> GetLeagues()
        {
            List<AdvancedLookup> lookups = new List<AdvancedLookup>();

            try
            {
                var q = from lookup in SkyLarkArenaEntities.Leauges
                        orderby lookup.LeagueName
                        select new
                        {
                            LookLookupValue = lookup.id,
                            LookupDescription = lookup.LeagueName
                        };

                if (q != null)
                {
                    Array.ForEach(q.ToArray(), l =>
                    {
                        lookups.Add(new AdvancedLookup(l.LookLookupValue, l.LookupDescription));
                    });
                }
            }
            catch (Exception ex)
            {
                throw new EntityContextException("GetLeagues failed.", ex);
            }

            return lookups;
        }






        public SkyLarkArenaEntities _SkyLarkArenaEntities;
        public SkyLarkArenaEntities SkyLarkArenaEntities
        {
            get
            {
                if (_SkyLarkArenaEntities == null)
                {
                    try
                    {
                        _SkyLarkArenaEntities = new SkyLarkArenaEntities();
                    }
                    catch (Exception ex)
                    {
                        throw new EntityContextException("UniteCms Entities Could not be created", ex);
                    }
                }

                return _SkyLarkArenaEntities;
            }
        }

    }
}
