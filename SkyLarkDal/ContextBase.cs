﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure.Interception;



namespace SkyLarkArenaDal
{
    public abstract class ContextBase :DbContext
    {


        protected static string soccerConnectionString;

        public static string SoccerConnectionString
        {
            set { soccerConnectionString = value; }

            
        }

 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<player>()

                .Map(m => m.Requires("isDeleted").HasValue(false))

                .Ignore(m => m.isDeleted);

            modelBuilder.Entity<prize>()

                .Map(m => m.Requires("isDeleted").HasValue(false))

                .Ignore(m => m.isDeleted);

            modelBuilder.Entity<district>()

                .Map(m => m.Requires("isDeleted").HasValue(false))

                .Ignore(m => m.isDeleted);



            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

        }

       
    }
}
