﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal.Models
{
   public  class UserData
    {

        public string fullName { get; set; }
        public Guid userId { get; set; }
        public string userName { get; set; }
    
        public int accessLevel { get; set; }
        public UserData()
        {
            fullName = "Unknown";
            userId = Guid.Empty;
            userName = "";
            
        }
        

    }
}
