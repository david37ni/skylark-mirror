﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyLarkArenaDal;
namespace SkyLarkArenaDal.Models
{
    public class TeamsList
    {
        public Guid id { get; set; } //(uniqueidentifier, not null)
        public string name { get; set; } //(nvarchar(250), null)
        public string description { get; set; } //(nvarchar(250), null)
        public Guid playerId { get; set; } //(uniqueidentifier, null)
        public int districtId { get; set; } //(int, null)
        public int leagueId { get; set; } //(int, null)
        public bool isActive { get; set; } //(bit, null)
        public string author { get; set; } //(varchar(50), null)
        public DateTime dateCreated { get; set; } //(date, null)
        public string authUserName { get; set; } //(varchar(50), null)
        public string authdate { get; set; } //(varchar(50), null)
        public Guid siteId { get; set; } //(uniqueidentifier, null)
        public bool isDeleted { get; set; } //(bit, null)
        public DateTime deletedDate { get; set; } //(date, null)
        public bool isArchived { get; set; } //(bit, null)
        public DateTime archivedDate { get; set; } //(date, null)

        public List<player> players = new List<player>();


    }

}
