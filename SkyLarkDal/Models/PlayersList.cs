﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal.Models
{
    public class PlayerList
    {

        public PlayerList()
        {

        }
        public PlayerList(string Name, string Player_id, string Description, string Gender,
            string Town, string Zipcode, string TeamName,string Telephone, string Email, bool Active, DateTime CreateDate)
        {
            name = Name;
            player_id = Player_id;
            description = Description;
            gender = Gender;
            town = Town;
            telephone = Telephone;
            email = Email;
            active = Active;
            createdDate = CreateDate;
            teamname = TeamName;

        }
        public string name { get; set; } //(varchar(150), null)
        public string player_id { get; set; } //(varchar(50), null)
        public string description { get; set; } //(varchar(max), null)
        public string address { get; set; } //(varchar(150), null)
        public string gender { get; set; } //(varchar(30), null)
        public string town { get; set; } //(varchar(150), null)
        public string zipcode { get; set; } //(varchar(50), null)
        public string telephone { get; set; } //(varchar(255), null)

        public string teamname { get; set; } //(varchar(255), null)
       
        public string email { get; set; } //(varchar(255), null)
        public bool active { get; set; } //(bit, null)
        public DateTime createdDate { get; set; } //(date, null)
        public string author { get; set; } //(varchar(255), null)
        public Guid siteid { get; set; } //(uniqueidentifier, null)
        public bool regEmailSent { get; set; } //(bit, null)
        public bool regEmailActivated { get; set; } //(bit, null)
        public Guid teamId { get; set; } //(uniqueidentifier, null)
        public bool isDeleted { get; set; } //(bit, null)
        public DateTime deletedDate { get; set; } //(date, null)
        public DateTime modifiedDate { get; set; } //(date, null)
        public string modifiedBy { get; set; } //(nvarchar(250), null)
        public bool isArchived { get; set; } //(bit, null)
        public Guid districtId { get; set; } //(uniqueidentifier, null)
        public DateTime dob { get; set; } //(date, null)


    }

}
