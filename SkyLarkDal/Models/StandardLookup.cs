﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyLarkArenaDal;
namespace SkyLarkArenaDal.Models
{

    public class StandardLookup
    {
        public StandardLookup(string lookupValue, string lookupDescription)
        {
            LookupValue = lookupValue;
            LookupDescription = lookupDescription;
        }

        public string  LookupValue { get; set; }
        public string LookupDescription { get; set; }


    }
}
