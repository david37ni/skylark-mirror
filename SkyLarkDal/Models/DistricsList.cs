﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyLarkArenaDal.Models
{
     
        public class DistricsList
    {
        public DistricsList(string name, int prefix)
        {
            Name = name;
            Prefix = prefix;
        }
        
        public string  Name { get; set; }
              public int Prefix { get; set; }

        public string SiteId { get; set; }

 
    }
}
