//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SkyLarkArenaDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class notificationsSetting
    {
        public System.Guid id { get; set; }
        public string email { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<System.Guid> accountType { get; set; }
        public Nullable<System.Guid> notificationType { get; set; }
        public Nullable<bool> urgent { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<System.DateTime> deletedDate { get; set; }
        public Nullable<System.DateTime> authCreatedDate { get; set; }
        public string authUserName { get; set; }
        public Nullable<System.Guid> userId { get; set; }
    }
}
